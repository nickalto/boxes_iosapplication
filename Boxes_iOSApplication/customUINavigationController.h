//
//  customUINavigationController.h
//  Boxes_iOSApplication
//
//  Created by NICK ALTO on 2/4/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "gameLogic.h"
#import "gameOptionViewController.h"


@interface customUINavigationController : UINavigationController
{
    UILabel *p1name;            // first player's name
    UILabel *p1score;           // first player's score
    UILabel *p2score;           // second player's score
    UILabel *p2name;            // second player's name
    CGRect framep1;             // frame from p1Name
    CGRect framep1score;        // frame for p1score
    CGRect framep2score;        // frame for p2score
    CGRect framep2;             // frame for p2Name
    CGRect turnFrame;           // frame for turnIndicator
}

//Synthsized Variables
@property CGRect framep1;
@property CGRect framep2;
@property CGRect framep1score;
@property CGRect framep2score;
@property CGRect turnFrame;


@end
