//
//  InfoViewController.m
//  Boxes_iOSApplication
//
//  Created by NICK ALTO on 5/29/12.
//  Copyright (c) 2012 Oregon State University. All rights reserved.
//

#import "InfoViewController.h"

@interface InfoViewController ()
{
    id viewController;
    CATransition *animation;
    int currentInt;
    int originatingVC;
}
@end

@implementation InfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andInt:(int)instructionNumber andVC:(id)controller
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        currentInt = instructionNumber;
        viewController = controller;
               
        animation = [CATransition animation];

    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    if(IS_IPHONE5)
        [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"i5.png"]]];
    else
        [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"plainBackground.jpg"]]];
    
    UIFont *sFont = [UIFont fontWithName:@"FFFTusj-Bold" size:15];
    UIFont *lFont = [UIFont fontWithName:@"FFFTusj-Bold" size:18];
    
    if(currentInt == 1)
    {
        i1l1.font = i1l2.font = i1l3.font = i1l4.font = i1l5.font = i1l6.font = sFont;
        i1g1.image = [UIImage imageNamed:@"inst1_img@2x.png"];
    }
    else if(currentInt == 2)
    {
        i1l1.font = i1l2.font = i1l3.font = i1l4.font = i1l5.font = i1l6.font = sFont;
        i1g1.image = [UIImage imageNamed:@"inst2_1.png"];
        i1g2.image = [UIImage imageNamed:@"inst2_2.png"];
        i1g1.alpha = 1;
    }
    else if(currentInt == 3)
    {
        i1l1.font = i1l2.font = i1l3.font = i1l4.font = i1l6.font = sFont;
        i1g1.image = [UIImage imageNamed:@"inst3_1.png"];
        i1l5.font = lFont;
    }
    else if(currentInt == 4)
    {
        i1l1.font = i1l2.font = i1l3.font = i1l5.font = i1l6.font  = i1l7.font = i1l8.font = sFont;
        i1l4.font = lFont;
    }
    

    UIImage *backImage = [UIImage imageNamed:@"backIconBrown.png"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 0, 40, 40);
    
    
    [backButton setImage:backImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = backBarButtonItem;
        
        
    //}
    UIImage *forwardImage = [UIImage imageNamed:@"forwardIconBrown.png"];
    UIButton *forwardButton = [UIButton buttonWithType:UIButtonTypeCustom];
    forwardButton.frame = CGRectMake(280, 0, 40, 40);
    
    [forwardButton setImage:forwardImage forState:UIControlStateNormal];
    
    UIBarButtonItem *forwardBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:forwardButton];
    
    [forwardButton addTarget:self action:@selector(nextScreen:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = forwardBarButtonItem;
    if(currentInt == 4)
        self.navigationItem.rightBarButtonItem = nil;

    [exit setBackgroundImage:[UIImage imageNamed:@"logo.png"] forState:UIControlStateNormal];
    
    [exit setBackgroundImage:[UIImage imageNamed:@"logoSelected.png"] forState:UIControlStateHighlighted];

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(IBAction)exit:(id)sender
{
    
    [animation setDelegate:self];
    [animation setDuration:0.5];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
    animation.type = @"pageCurl";
    
    animation.subtype = kCATransitionFromTop;
    animation.fillMode = kCAFillModeForwards;
    animation.startProgress = 0;
    [animation setRemovedOnCompletion:YES];
    
    [self.navigationController.view.layer addAnimation:animation forKey:@"pageCurlAnimation"];

    [self.navigationController popToViewController:viewController animated:NO];

}

-(void)backButton:(id)sender
{
    [animation setDelegate:self];
    [animation setDuration:0.5];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
    if( currentInt == 1 ) {
        animation.type = @"pageCurl";
    } else {
        animation.type = @"pageUnCurl";
    }
    animation.subtype = kCATransitionFromTop;
    animation.fillMode = kCAFillModeForwards;
    animation.startProgress = 0;
    [animation setRemovedOnCompletion:YES];
    if(currentInt == 1){
        [self.navigationController.view.layer addAnimation:animation forKey:@"pageCurlAnimation"];
    } else {
        [self.navigationController.view.layer addAnimation:animation forKey:@"pageUnCurlAnimation"];
    }
    [self.navigationController popViewControllerAnimated:NO];
    
}

-(IBAction)nextScreen:(id)sender
{
    InfoViewController *next;
    if(currentInt > 4)
        NSLog(@"error next Screen\n");
    else {
        if(currentInt == 0)
        {
             next = [[InfoViewController alloc]initWithNibName:@"InfoViewController" bundle:nil andInt:currentInt+1 andVC:viewController];
        }
        else if(currentInt == 1)
        {
            next = [[InfoViewController alloc]initWithNibName:@"InfoViewController-II" bundle:nil andInt:currentInt+1 andVC:viewController];

        }
        else if(currentInt == 2)
        {
            next = [[InfoViewController alloc]initWithNibName:@"InfoViewController-III" bundle:nil andInt:currentInt+1 andVC:viewController];
        }
        else if(currentInt == 3)
        {
            next = [[InfoViewController alloc]initWithNibName:@"InfoViewController-IV" bundle:nil andInt:currentInt+1 andVC:viewController];
        }
    self.navigationController.view.autoresizesSubviews = FALSE;
        [animation setDelegate:self];
        [animation setDuration:0.5];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
        animation.type = @"pageCurl";
        
        animation.subtype = kCATransitionFromBottom;
        animation.fillMode = kCAFillModeBackwards;
        animation.startProgress = 0;
        [animation setRemovedOnCompletion:YES];
        
        [self.navigationController.view.layer addAnimation:animation forKey:@"pageCurlAnimation"];

        
    [self.navigationController pushViewController:next animated:NO];
    }
}

@end
