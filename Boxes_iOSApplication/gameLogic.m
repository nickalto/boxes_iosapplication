//
//  gameLogic.m
//  Boxes_iOSApplication
//
//  Created by NICK ALTO on 1/30/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//
#import "gameLogic.h"
BOOL GAMELOGICPRINT = FALSE;

@implementation gameLogic

@synthesize player1             = _player1;
@synthesize player2             = _player2;
@synthesize playerTurn          = _playerTurn;
@synthesize xDimension          = _xDimension;
@synthesize yDimension          = _yDimension;
@synthesize p1Initials          = _p1Initials;
@synthesize p2Initials          = _p2Initials;
@synthesize alreadyStartedGame  = _alreadyStartedGame;
@synthesize dot                 = _dot;
@synthesize initialTouch        = _initialTouch;
@synthesize boxbotflag          = _boxbotflag;
@synthesize finalTouch          = _finalTouch;
@synthesize xLineArr            = _xLineArr;
@synthesize yLineArr            = _yLineArr;
@synthesize dotArr              = _dotArr;
@synthesize p1Score             = _p1Score;
@synthesize p2Score             = _p2Score;
@synthesize firstTouch          = _firstTouch;
@synthesize lastTouch           = _lastTouch;
@synthesize indexToUpdateWith   = _indexToUpdateWith;
@synthesize updateLineArray     = _updateLineArray;
@synthesize passedDotLocation   = _passedDotLocation;
@synthesize lineLogicDot        = _lineLogicDot;
@synthesize arrOfLabels         = _arrOfLables;
@synthesize arrHelper           = _arrHelper;
@synthesize playAgain           = _playAgain;
@synthesize reload              = _reload;
@synthesize refreshBoard        = _refreshBoard;
@synthesize onlineGame          = _onlineGame;
@synthesize initialMove         = _initialMove;
@synthesize gameDataID          = _gameDataID;

- (id)init
{
    self = [super init];
    if (self) {
        //Initialize all synthesized variables to 0/nil
        //In the case that there's already a game started create 
        player2             = nil;
        player1             = nil;
        p1Initials          = nil;
        p2Initials          = nil;
        playerTurn          = 0;
        xDimension          = 0;
        yDimension          = 0;
        alreadyStartedGame  = 0;
        playerTurn          = 1;
        indexToUpdateWith   = 0;
        updateLineArray     = -1;
        playAgain           = 0;
        reload              = 0;
        refreshBoard        = 0;
        onlineGame          = 0;
    }
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(calculateClosestDots:)
     name:@"touchesBeganCanvas"
     object:nil];
    
    return self;
}

/**************************************************************************************
 * Purpose: when passed in the gameboard setUpDots will allocate and initialize all 
 * the dots in the curent gameboard, setting up thier dotID. Although this function 
 *
 * Refrence: Called by gameBoardViewController to initialize the dots. Keep in mind 
 * the dotLocation for each dot is set in the gameBoardViewController after this function
 * is called.
 **************************************************************************************/

- (NSMutableArray *)setUpDots:(gameLogic *)gb
{
    int i, j, g_dID;
    int x = gb.xDimension;
    int y = gb.yDimension;
    if(!gb.reload)
    {
        gb.xLineArr = [[NSMutableArray alloc]initWithCapacity:( (x-1) * (y) )];
        gb.yLineArr = [[NSMutableArray alloc]initWithCapacity:( (x) * (y-1) )];
        //Initialize xLineArr with NSNumbers of 0
        NSNumber *num;
        for(i=0; i< ((x-1)*y); i++)
        {
            num = [[NSNumber alloc]initWithBool:FALSE];
            [gb.xLineArr addObject:num];
        }
        //Same for yLineArr
        for(i=0; i < (x*(y-1)); i++)
        {
            num = [[NSNumber alloc]initWithBool:FALSE];
            [gb.yLineArr addObject:num];
            
        }
    }    
    //Error checking arguments
    if( (x == 0) || (y == 0) )
    if(GAMELOGICPRINT)
        NSLog(@"ERROR: Dimensions haven't been set up");
    dotArr = [[NSMutableArray alloc]init];
    
    g_dID = 0;
    
    for (i = 0; i < y; i++)
    {
        for(j = 0; j < x; j++)
        {
            dot = [[Dot alloc]initWithID:g_dID];
            g_dID++;
            
            dot.left  = 1;
            dot.right = 1;
            dot.up    = 1;
            dot.down  = 1;
            
            dot.gameBoard = gb;
            if(GAMELOGICPRINT)
                NSLog(@"Dot ID gl: %d\n", dot.dotID);
            //Tested with both 5x6 grids and 7x8 grids on 1/31/12
            //working as it should be.
            if((j == 0) || ((j % x) == 0 )) //dot has no left connection
                dot.left = 0;
            if(i == 0)                      //dot has no up connection
                dot.up = 0;
            if(i == y-1)                    //dot has no down connection
                dot.down = 0;
            if((j % x) == (x-1))            //dot has not right connection
                dot.right = 0;          
            
            dot.xDim = gb.xDimension;
            
            [dotArr addObject:dot];
            
        }
    }
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(lineSnappingAndDrawing:)
     name:@"touchesEndedForDot"
     object:nil];
    
    return dotArr;
    
}

/**************************************************************************************
 * Purpose: Given a dot, which also contains it's own gameboard we are able to determine
 * valid moves, making sure that the user isn't making illegal moves and that we are 
 * only drawing to other dots. On top of that it makes sure that we are drawing to the 
 * center of the dots and if it's a valid move sends out a notification to customUIView
 * function updateLine which sets dots to be drawn. Passed in dot is the first dot 
 * touched and endLocation (GCPoint), a variable within that dot should be set to the 
 * second dot touched.
 *
 * Function: Called as a notification by posting "touchesEndedForDot", and passing in
 * a Dot object with the dotlocation is the CGPoint of the beginning or first dot and 
 * endLocatin is the CGPoint of the final dot you want to draw to. Called by gameBoard
 * viewController and also indirectly by customUIView ->calcualteClosestDots->here.
 **************************************************************************************/

-(int)lineAbove:(gameLogic *)game X:(BOOL)isX YOnlyRightOrLeft:(int)right index:(int)index
{
    int above = -1;
    int x = game.xDimension-1;
    int y = game.yDimension-1;
    int modindex = index % y;
    int column = index/y;
    
    if(isX)
        ((index - x) > 0) ? (above = index - x) : (above = -1);
    else
    {
        if(right) //right
            ((index+1) % y) ? (above = (column * x) + modindex): (above = -1);
        else
            (index % y) ? (above = (column * x) + modindex - 1): (above = -1);
    }
    if(above != -1 && [game.xLineArr[above] intValue])
        return above;
    else
        return -1;

}

-(int)lineBelow:(gameLogic *)game X:(BOOL)isX YOnlyRightOrLeft:(int)right index:(int)index
{
    int below = -1;
    int x = game.xDimension-1;
    int y = game.yDimension-1;
    int modindex = index % y;
    int column = (index/y)+1;
    
    if(isX)
        ((index + x) < ((x * game.yDimension)-1)) ? (below = index + x) : (below = -1);
    else
    {
        if(right) 
            ((index+1) % y) ? (below = (column * x) + modindex): (below = -1);
        else
            ((index) % y) ? (below = (column * x) + modindex - 1): (below = -1);
    }
    
    if(below != -1 && [game.xLineArr[below] intValue])
        return below;
    else
        return -1;
}

-(int)lineRight:(gameLogic *)game X:(BOOL)isX XOnlyAboveOrBelow:(int)above index:(int)index
{
    int right = -1;
    int x = game.xDimension-1;
    int y = game.yDimension-1;
    int rowcalc = (index / x);
    int row = (rowcalc > 0) ? (rowcalc - 1) : (rowcalc);
    int modindex = index % x;
    
    if(isX)
    {
        if(above)
            right = (row * y) + modindex + 1;
        else
            ((rowcalc * y) + modindex + 1) > ((game.xDimension * y)-1) ? (right = -1) : (right =  (rowcalc * y) + modindex + 1);
    }
    else
        ((index+1) % y) ? (right = index + 1) : (right = -1);
    
    if(right != -1 && [game.yLineArr[right] intValue])
        return right;
    else
        return -1;
    
}

-(BOOL)lineLeft:(gameLogic *)game X:(BOOL)isX XOnlyAboveOrBelow:(int)above index:(int)index
{
    int left = -1;
    int x = game.xDimension-1;
    int y = game.yDimension-1;
    int rowcalc = (index / x);
    int row = (rowcalc > 0) ? (rowcalc - 1) : (rowcalc);
    int modindex = index % x;
    
    if(isX)
    {
        if(above)
            left = (row * y) + modindex;
        else
            ((rowcalc * y + modindex) > ((game.yDimension * x)-1)) ? (left = -1) : (left =  (rowcalc * y) + modindex);
    }
    else
        (index % y) ? (left = index - 1) : (left = -1);

    if((left != -1) && [game.yLineArr[left] intValue])
        return left;
    else
        return -1;
}


-(void)AI:(gameLogic*)game
{
    int backup = -1;
    BOOL backupbool = FALSE;
     for(int i = 0 ; i < ((game.xDimension-1) * (game.yDimension)); i++)
     {
         if(![[game.xLineArr objectAtIndex:i]intValue])
         {             
             int leftabove = [self lineLeft:game X:YES XOnlyAboveOrBelow:1 index:i];
             int leftbelow = [self lineLeft:game X:YES XOnlyAboveOrBelow:0 index:i];
             int rightabove =[self lineRight:game X:YES XOnlyAboveOrBelow:1 index:i];
             int rightbelow =[self lineRight:game X:YES XOnlyAboveOrBelow:0 index:i];
             int linebelow = [self lineBelow:game X:YES YOnlyRightOrLeft:1 index:i];
             int lineabove = [self lineAbove:game X:YES YOnlyRightOrLeft:1 index:i];
             if(GAMELOGICPRINT)
             {
                 NSLog(@"1st pass\n");
                 NSLog(@"leftabove = %d, rightabove = %d, lineabove = %d\n", leftabove,rightabove, lineabove);
                 NSLog(@"rightbelow = %d, leftbelow = %d, linebelow = %d\n", rightbelow,leftbelow, linebelow);
             }
             if((leftabove != -1) && (lineabove != -1) && (rightabove != -1))
             {
                 [self setLocationsForLine:game X:TRUE index:i];
                 return;
             }
             else if ((leftbelow != -1) && (rightbelow != -1) && (linebelow != -1))
             {
                 [self setLocationsForLine:game X:TRUE index:i];
                 return;
             }
             else if((leftabove == -1 && rightabove == -1 && leftbelow == -1 && rightbelow == -1))
             {
                 backup = i;
                 backupbool = TRUE;
             }
             else if(backup == -1 && linebelow == -1 && lineabove == -1 && (leftabove == -1 || rightabove == -1 || leftbelow == -1 || rightbelow == -1))
             {
                 backup = i;
                 backupbool = TRUE;
             }
         }
     }
    
    for(int i = 0; i < ((game.xDimension) * (game.yDimension -1)); i++)
    {
        
        if(![[game.yLineArr objectAtIndex:i] intValue])
        {
            int leftabove = [self lineAbove:game X:NO YOnlyRightOrLeft:0 index:i];
            int rightabove = [self lineAbove:game X:NO YOnlyRightOrLeft:1 index:i];
            int leftbelow =[self lineBelow:game X:NO YOnlyRightOrLeft:0 index:i];
            int rightbelow =[self lineBelow:game X:NO YOnlyRightOrLeft:1 index:i];
            int lineright = [self lineRight:game X:NO XOnlyAboveOrBelow:0 index:i];
            int lineleft = [self lineLeft:game X:NO XOnlyAboveOrBelow:0 index:i];
            if(GAMELOGICPRINT)
            {
                NSLog(@"2nd pass\n");
                NSLog(@"leftabove = %d, lineleft = %d, leftbelow = %d\n", leftabove,lineleft, leftbelow);
                NSLog(@"rightabove = %d, lineright = %d, rightbelow = %d\n", rightabove,lineright, rightbelow);
            }
            if((leftabove != -1) && (leftbelow != -1) && (lineleft != -1))
            {
                [self setLocationsForLine:game X:FALSE index:i];
                return;
            }
            else if((rightabove != -1) && (rightbelow != -1) && (lineright != -1))
            {
                [self setLocationsForLine:game X:FALSE index:i];
                return;
            }
            else if( leftabove == -1 && leftbelow == -1 && rightabove == -1 && lineright == -1)
            {
                backup = i;
                backupbool = FALSE;
            }
            else if( backup == -1 && lineleft == -1 && lineright == -1 && (leftbelow == -1 || leftabove == -1))
            {
                backup = i;
                backupbool = FALSE;
            }
        }
    }

    if(backup != -1)
    {
        if(GAMELOGICPRINT)
            NSLog(@"USING BACKUP = %d\n", backup);
        [self setLocationsForLine:game X:backupbool index:backup];
        return;
    }
    if(GAMELOGICPRINT)
        NSLog(@"USING RANDOMIZED\n");

    NSInteger r = arc4random() % 16;
    NSInteger rand = arc4random() % 16;
    if( rand > 8)
    {
        for(; r < ((game.xDimension) * (game.yDimension -1)); r++)
        {

            if(![[game.yLineArr objectAtIndex:r] intValue])
            {
                [self setLocationsForLine:game X:FALSE index:r];
                return;
            }
        }
    }
    else
    {
        for(; r < ((game.xDimension-1) * (game.yDimension)); r++)
        {
             if(![[game.xLineArr objectAtIndex:r]intValue])
            {
                [self setLocationsForLine:game X:TRUE index:r];
                return;
            }
        }
    }
    
    //•Check to see if you can complete a box
    //•If you can't complete a box pick random area
    //•If that doesn't set the player up to fill a box place move
    //•Otherwise add some logic for smarter moves when it comes to allowing
    //opponet to fill boxes.
}
-(void) setLocationsForLine:(gameLogic *)game X:(BOOL)isX index:(int)index
{
    int x = game.xDimension-1;
    int y = game.yDimension-1;
    int rowcalc = (index / x);
    CGPoint first;
    CGPoint second;
    if(GAMELOGICPRINT)
        NSLog(@"BoxBot Move - index = %d\n", index);
    if(isX)
    {
        int dotindex = index + rowcalc;
        first = [[game.dotArr objectAtIndex:dotindex] dotLocation];
        second = [[game.dotArr objectAtIndex:dotindex+1]dotLocation];
    }
    else
    {
        first = [[game.dotArr objectAtIndex:index] dotLocation];
        second = [[game.dotArr objectAtIndex:index+y] dotLocation];
    }
    CGPoint temp = CGPointMake(abs((first.x + second.x)/2), abs((first.y + second.y)/2));
    game.passedDotLocation = first;
    game.lastTouch = second;
    game.updateLineArray = 1;
    NSValue* val = [NSValue valueWithCGPoint:temp];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"touchesBeganCanvas"
     object:val];
}

-(void)lineSnappingAndDrawing:(NSNotification *)notification
{
    Dot* passedDot = [notification object];
    if(GAMELOGICPRINT)
        NSLog(@"line snapping dotid= %d\n", passedDot.dotID);
    
    gameLogic *dotGameBoard = passedDot.gameBoard;
    if(GAMELOGICPRINT)
    {
       /* for(int i = 0 ; i < ((dotGameBoard.xDimension-1) * (dotGameBoard.yDimension)); i++)
        {
            NSLog(@"line snapping - xLineArray[%d] = %d", i, [[dotGameBoard.xLineArr objectAtIndex:i]intValue] );
        }
        */
    }
    dotGameBoard.passedDotLocation = passedDot.dotLocation;
    CGFloat xBegin, yBegin, xEnd, yEnd, xOffset, yOffset, largestOffset;
    CGPoint newEndLocation;                         //Float used to store the final ending
    newEndLocation.x    = 0;                        //location, sent to customUIView
    newEndLocation.y    = 0;               
    int drawFlag        = 1;                        //flag used to control the drawing of lines
    int iconSize        = 0;                       //variable set to 1/2 the width of the dot icon
    xBegin              = passedDot.dotLocation.x;  //beginning dot x location
    yBegin              = passedDot.dotLocation.y;  //beginning dot y location
    xEnd                = passedDot.endLocation.x;  //ending dot x location
    yEnd                = passedDot.endLocation.y;  //ending dot y location
    
    //error checking to make sure that we can draw
    if((abs(xBegin - xEnd) < 10)  && (abs(yBegin - yEnd) < 10))
        drawFlag = 0;
    if(passedDot.left == 0 && xEnd < (xBegin - iconSize))
        drawFlag = 0;
    if(passedDot.right == 0 && xEnd > (xBegin + iconSize))
        drawFlag = 0;
    if(passedDot.up == 0 && yEnd < (yBegin - iconSize))
        drawFlag = 0;
    if(passedDot.down == 0 && yEnd > (yBegin + iconSize))
        drawFlag = 0;
    
    //for x, negative xOffset means go to right
    //for x, positive xOffset means go to left
    //for y, negative yOffset means go down
    //for y, positive yOffset means go up
    
    if(xEnd < xBegin)
        xOffset = (xBegin - iconSize) - xEnd;
    else
        xOffset = (xBegin + iconSize) - xEnd;
    if(yEnd < yBegin)
        yOffset = (yBegin - iconSize) - yEnd;
    else
        yOffset = (yBegin + iconSize) - yEnd;
    
    //largest offset 0 for x, 1 for y
    
    if(abs(xOffset) > abs(yOffset))
        largestOffset = 0;
    else 
        largestOffset = 1;
    
    
    //take the difference between xOffset and yOffset
    //whichever one is larger, that's the direction they're 
    //trying to go. 
    if(GAMELOGICPRINT)
        NSLog(@"PLAYER %d's TURN\n", dotGameBoard.playerTurn );
    
    if((drawFlag) && (largestOffset == 0)) //drawing either right or left
    {
        
        if((xOffset > 0) && (passedDot.left == 1)) // go left
        {
            int calc = passedDot.dotID/passedDot.xDim;
            NSNumber *tempNum = [dotGameBoard.xLineArr objectAtIndex:(passedDot.dotID - (calc + 1))];
            if(GAMELOGICPRINT)
                NSLog(@" tempNUM = %d\n", [tempNum integerValue]);
            
            if(![tempNum integerValue]) //value is zero meaning not connected
            {
                Dot *temp = [dotArr objectAtIndex:passedDot.dotID - 1];
                newEndLocation = temp.dotLocation;
                dotGameBoard.updateLineArray = 0;
                dotGameBoard.indexToUpdateWith = (passedDot.dotID - (calc + 1));
                if(GAMELOGICPRINT)
                    NSLog(@"DREW LINE FROM DOT %d to %d\n",passedDot.dotID, temp.dotID );
            }
            else  // Means it's already been connected, don't draw another line.
                drawFlag = 0;

        }
        else if((xOffset < 0)  && (passedDot.right == 1)) //go right
        {
            int calc = passedDot.dotID/passedDot.xDim;
            
            NSNumber *tempNum = [dotGameBoard.xLineArr objectAtIndex:(passedDot.dotID - calc )];
            
            if(![tempNum integerValue]) //value is zero meaning not connected
            {
                Dot *temp = [dotArr objectAtIndex:passedDot.dotID + 1];
                newEndLocation = temp.dotLocation;
                dotGameBoard.updateLineArray = 0;
                dotGameBoard.indexToUpdateWith = (passedDot.dotID - calc);
            }
            else  // Means it's already been connected, don't draw another line.
                drawFlag = 0;
        }
    }
    
    if((drawFlag) && (largestOffset == 1))
    {
        if((yOffset > 0) && (passedDot.up == 1)) // go up
        {
            NSNumber *tempNum = [dotGameBoard.yLineArr objectAtIndex:(passedDot.dotID - passedDot.xDim )];
            
            if(![tempNum integerValue]) //value is zero meaning not connected
            {
                
                Dot *temp = [dotArr objectAtIndex:(passedDot.dotID - passedDot.xDim)];
                if(GAMELOGICPRINT)
                    NSLog(@"temp dot id %d\n", temp.dotID);
                newEndLocation = temp.dotLocation;
                dotGameBoard.updateLineArray = 1;
                dotGameBoard.indexToUpdateWith = (passedDot.dotID - passedDot.xDim);
                
            }
            else  // Means it's already been connected, don't draw another line.
                drawFlag = 0;
        }
        else if((yOffset < 0) && (passedDot.down == 1)) // go down
        {
            NSNumber *tempNum = [dotGameBoard.yLineArr objectAtIndex:(passedDot.dotID)];
            
            if(![tempNum integerValue]) //value is zero meaning not connected
            {
                
                Dot *temp = [dotArr objectAtIndex:(passedDot.dotID + passedDot.xDim)];
                if(GAMELOGICPRINT)
                    NSLog(@"temp dot id %d\n", temp.dotID);
                newEndLocation = temp.dotLocation;
                dotGameBoard.updateLineArray = 1;
                dotGameBoard.indexToUpdateWith = passedDot.dotID;
            }
            else  // Means it's already been connected, don't draw another line.
                drawFlag = 0;
        }
        
    }
    
    if(newEndLocation.x == 0 || newEndLocation.y == 0)
        drawFlag = 0;

    dotGameBoard.lineLogicDot = passedDot;
    dotGameBoard.firstTouch = passedDot.dotLocation;
    dotGameBoard.lastTouch = newEndLocation;
    
    if(drawFlag)
    {
        NSValue *val1 = [NSValue valueWithCGPoint:passedDot.dotLocation];
        NSValue *val2 = [NSValue valueWithCGPoint:newEndLocation];
        NSMutableArray *arr = [[NSMutableArray alloc] initWithObjects:val1, val2, nil];
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"calculatedDots"
         object:arr];
    }

}


/**************************************************************************************
 * Purpose: lineLogic is the main algorithm that determines whether a box has been won
 * and by what user, it also contains the logic that determines valid legal moves and 
 * triggers when the game is won/lost/draw. It is the core logic for keeping track of 
 * which moves have already been played, and oversees the gameboard.
 *
 * Reference: lineLogic is refrenced by gameBoardViewController when the user hits the 
 * verify/submit button it submits thier move and updates the game accordingly. Passed
 * in the gameBoard instance. 
 **************************************************************************************/

-(NSMutableArray *)lineLogic:(gameLogic *)dotGameBoard
{
    if(GAMELOGICPRINT)
    {
    for(int i = 0 ; i < ((dotGameBoard.xDimension-1) * (dotGameBoard.yDimension)); i++)
    {
        NSLog(@"xLineArray[%d] = %d", i, [[dotGameBoard.xLineArr objectAtIndex:i]intValue] );
    }
    }
    CGPoint newEndLocation;
    Dot* passedDot          = dotGameBoard.lineLogicDot;
    int anotherTurn         = 0;
    int drawFlag            = 1;
    int previousMove        = dotGameBoard.indexToUpdateWith;
    int previousArray       = dotGameBoard.updateLineArray; //0 for x  - 1 for y
    int illegalmove         = 0;
    newEndLocation.x        = 0;
    newEndLocation.y        = 0;
    int topLine;
    CGPoint upperLeftDot;
    CGPoint lowerRightDot;
    if(GAMELOGICPRINT)
    {
        NSLog(@"VALUE OF ILLEGAL MOVE = %d\n", illegalmove);
        NSLog(@"\n\n ***** PLAYER %d's TURN ***** \n\n", dotGameBoard.playerTurn);
    }
    int helper;
    int helper2;
    int helper3;
    int helper4;
    if(!illegalmove && drawFlag ) //valid move
    {
        if(GAMELOGICPRINT)
            NSLog(@"\n\nStarting box Algorithm\n\n");
        if(previousArray == 0) //last move was horizontal
        {
            if(previousMove / (dotGameBoard.xDimension -1)) //there is a possible box above line that was just drawn
            {
                if(GAMELOGICPRINT)
                    NSLog(@"\nBOX ABOVE: Box above this line\n");
                //first check lines above current line starting with upper left, above and right.
                // if previousMove is the bottom line of a box, this is checking for left line.
                helper = previousMove/(dotGameBoard.xDimension-1);
                helper2 = (helper-1) * (dotGameBoard.yDimension-1);
                helper3 = previousMove%(dotGameBoard.xDimension-1);
                helper4 = helper2 + helper3;
                NSNumber * temp = [dotGameBoard.yLineArr objectAtIndex:(helper4)];
                if([temp intValue])
                {
                    helper = previousMove - (dotGameBoard.xDimension-1);
                    topLine = helper;
                    if(GAMELOGICPRINT)
                        NSLog(@"\nBOX ABOVE: Upper left Connected\n");
                    //check top line, to see if it's connected 1 if connected 0 if not.
                    temp = [dotGameBoard.xLineArr objectAtIndex:(helper)];
                    if([temp intValue])
                    {
                        if(GAMELOGICPRINT)
                            NSLog(@"\nBOX ABOVE: TOP Connected\n");
                        //check the right side line, if this is connected then it's a box. 
                        helper = previousMove/ (dotGameBoard.xDimension-1);
                        helper2 = (helper-1) * (dotGameBoard.yDimension-1);
                        helper3 = previousMove%(dotGameBoard.xDimension-1);
                        helper4 = helper2 + helper3 + 1;
                        temp = [dotGameBoard.yLineArr objectAtIndex:(helper4)];
                        if([temp intValue])
                        {   
                            helper = topLine/(dotGameBoard.xDimension-1);
                            helper2 = helper * dotGameBoard.xDimension;
                            helper3 = topLine % (dotGameBoard.xDimension-1);
                            helper4 = helper2 + helper3;
                            upperLeftDot = [[dotGameBoard.dotArr objectAtIndex:helper4]dotLocation];
                            lowerRightDot = [[dotGameBoard.dotArr objectAtIndex:(helper4 + dotGameBoard.xDimension+1)]dotLocation];
                            CGRect a = CGRectMake(upperLeftDot.x, upperLeftDot.y, (lowerRightDot.x - upperLeftDot.x), (lowerRightDot.y - upperLeftDot.y));
                            NSNumber *turnLabel;
                            if(dotGameBoard.playerTurn == 1)
                                turnLabel = [[NSNumber alloc]initWithInt:1];
                            else
                                turnLabel = [[NSNumber alloc]initWithInt:2];
                            
                            NSValue *pointObj = [NSValue valueWithCGRect:a];
                            [dotGameBoard.arrOfLabels replaceObjectAtIndex:topLine withObject:pointObj];
                            [dotGameBoard.arrHelper replaceObjectAtIndex:topLine withObject:turnLabel];
                            
                            if(GAMELOGICPRINT)
                                NSLog(@"\n\n BOX ABOVE: A BOX WAS JUST CONNECTED!!\n\n");
                             //there is a box connected above the given line just connected
                            //figure out the top left dot in this box, find a way to create
                            //a UILabel from that corner to the bottom right dot. Fill with initials
                            if(dotGameBoard.playerTurn == 1)
                            {
                                dotGameBoard.p1Score += 1;
                                [dotGameBoard drawInitials:upperLeftDot width:(lowerRightDot.x - upperLeftDot.x) height:(lowerRightDot.y - upperLeftDot.y) player:1 andBoard:dotGameBoard];
                            }
                            else 
                            {
                                dotGameBoard.p2Score += 1;
                                [dotGameBoard drawInitials:upperLeftDot width:(lowerRightDot.x - upperLeftDot.x) height:(lowerRightDot.y - upperLeftDot.y) player:2 andBoard:dotGameBoard];
                            }                            
                            anotherTurn = 1;
                            
                        }
                    }
                }
            }
            if(passedDot.down == 1)// there is no box above so check to see if there is a box below
            {
                
                helper = previousMove/(dotGameBoard.xDimension-1);
                helper2 = helper * (dotGameBoard.yDimension-1);
                helper3 = previousMove%(dotGameBoard.xDimension-1);
                helper4 = helper3 + helper2;
                topLine = previousMove;
                NSNumber * temp = [dotGameBoard.yLineArr objectAtIndex: (helper4)];
                if([temp intValue]) //checking bottom left line to see if it is connected
                {
                    if(GAMELOGICPRINT)
                        NSLog(@"\nBOX BELOW: Bottom Left line\n");
                     helper = previousMove + (dotGameBoard.xDimension-1);
                    temp = [dotGameBoard.xLineArr objectAtIndex:(helper) ];
                    if([temp intValue]) //checking bottom line to see if connected
                    {
                        if(GAMELOGICPRINT)
                            NSLog(@"\nBOX BELOW: Bottom line\n");
                         helper = previousMove/(dotGameBoard.xDimension-1);
                        helper2 = helper * (dotGameBoard.yDimension-1);
                        helper3 = previousMove%(dotGameBoard.xDimension-1);
                        helper4 = helper2 + helper3 + 1; 
                        temp = [dotGameBoard.yLineArr objectAtIndex:(helper4) ];
                        if([temp intValue]) // checking to see if bottom right line is connected
                        {
                            helper = topLine/(dotGameBoard.xDimension-1);
                            helper2 = helper * dotGameBoard.xDimension;
                            helper3 = topLine % (dotGameBoard.xDimension-1);
                            helper4 = helper2 + helper3;
                            if(GAMELOGICPRINT)
                                NSLog(@"\n\n INDEX = %d\n\n", helper4);
                             upperLeftDot = [[dotGameBoard.dotArr objectAtIndex:helper4]dotLocation];
                            lowerRightDot = [[dotGameBoard.dotArr objectAtIndex:(helper4 + dotGameBoard.xDimension+1)]dotLocation];
                            lowerRightDot = [[dotGameBoard.dotArr objectAtIndex:(helper4 + dotGameBoard.xDimension+1)]dotLocation];
                            
                            CGRect a = CGRectMake(upperLeftDot.x, upperLeftDot.y, (lowerRightDot.x - upperLeftDot.x), (lowerRightDot.y - upperLeftDot.y));
                            NSNumber *turnLabel;
                            if(dotGameBoard.playerTurn == 1)
                                turnLabel = [[NSNumber alloc]initWithInt:1];
                            else
                                turnLabel = [[NSNumber alloc]initWithInt:2];
                            
                            NSValue *pointObj = [NSValue valueWithCGRect:a];
                            [dotGameBoard.arrOfLabels replaceObjectAtIndex:topLine withObject:pointObj];
                            [dotGameBoard.arrHelper replaceObjectAtIndex:topLine withObject:turnLabel];
                            
                            if(GAMELOGICPRINT)
                                NSLog(@"\n\n BOX BELOW: A BOX WAS JUST CONNECTED!!\n\n");
                             //there is a box connected below the given line just connected
                            //figure out the top left dot in this box, find a way to create
                            //a UILabel from that corner to the bottom right dot. Fill with initials
                            if(dotGameBoard.playerTurn == 1)
                            {
                                dotGameBoard.p1Score += 1;
                                [dotGameBoard drawInitials:upperLeftDot width:(lowerRightDot.x - upperLeftDot.x) height:(lowerRightDot.y - upperLeftDot.y) player:1 andBoard:dotGameBoard];
                            }
                            else 
                            {
                                dotGameBoard.p2Score += 1;
                                [dotGameBoard drawInitials:upperLeftDot width:(lowerRightDot.x - upperLeftDot.x) height:(lowerRightDot.y - upperLeftDot.y) player:2 andBoard:dotGameBoard];
                            }
                            
                            
                            anotherTurn = 1;
                        }
                    }
                }
                
            }
        }
        if(previousArray == 1) //last move was vertical
        {
            if(GAMELOGICPRINT)
                NSLog(@"\nBOX LEFT: Starting Box Left\n");
             
            //There was no box above or below, check the sides given that the line was drawn on the y axis
            //checking left then right.
            if(passedDot.left == 1)  
            {
                helper = (previousMove) / (dotGameBoard.yDimension-1);
                helper2 = (helper +1) * (dotGameBoard.xDimension-1);
                helper3 = previousMove % (dotGameBoard.yDimension-1);
                helper4 = (helper3 -1) + helper2;
                NSNumber *temp = [dotGameBoard.xLineArr objectAtIndex:helper4];
                if([temp intValue]) //checking bottom left line
                {
                    if(GAMELOGICPRINT)
                        NSLog(@"\nBOX LEFT: Bottom Left line\n");
                     helper = previousMove -1;
                    temp = [dotGameBoard.yLineArr objectAtIndex:helper];
                    if([temp intValue]) //checking far left line
                    {
                        if(GAMELOGICPRINT)
                            NSLog(@"\nBOX LEFT: Far Left line\n");
                         helper = previousMove / (dotGameBoard.yDimension -1);
                        helper2 = helper * (dotGameBoard.xDimension-1);
                        helper3 = previousMove % (dotGameBoard.yDimension-1);
                        helper4 = (helper3 - 1) + helper2;
                        topLine = helper4;
                        temp = [dotGameBoard.xLineArr objectAtIndex:helper4];
                        if([temp intValue]) //checking top left line
                        {
                            helper = topLine/(dotGameBoard.xDimension-1);
                            helper2 = helper * dotGameBoard.xDimension;
                            helper3 = topLine % (dotGameBoard.xDimension-1);
                            helper4 = helper2 + helper3;
                            if(GAMELOGICPRINT)
                                NSLog(@"\n\n INDEX = %d\n\n", helper4);
                             upperLeftDot = [[dotGameBoard.dotArr objectAtIndex:helper4]dotLocation];
                            lowerRightDot = [[dotGameBoard.dotArr objectAtIndex:(helper4 + dotGameBoard.xDimension+1)]dotLocation];
                            lowerRightDot = [[dotGameBoard.dotArr objectAtIndex:(helper4 + dotGameBoard.xDimension+1)]dotLocation];
                            
                            CGRect a = CGRectMake(upperLeftDot.x, upperLeftDot.y, (lowerRightDot.x - upperLeftDot.x), (lowerRightDot.y - upperLeftDot.y));
                            NSNumber *turnLabel;
                            if(dotGameBoard.playerTurn == 1)
                                turnLabel = [[NSNumber alloc]initWithInt:1];
                            else
                                turnLabel = [[NSNumber alloc]initWithInt:2];
                            
                            NSValue *pointObj = [NSValue valueWithCGRect:a];
                            [dotGameBoard.arrOfLabels replaceObjectAtIndex:topLine withObject:pointObj];
                            [dotGameBoard.arrHelper replaceObjectAtIndex:topLine withObject:turnLabel];
                            
                            if(GAMELOGICPRINT)
                                NSLog(@"\n\n BOX LEFT: A BOX WAS JUST CONNECTED!!\n\n");
                             //there is a box connected just left of the given line just connected
                            //figure out the top left dot in this box, find a way to create
                            //a UILabel from that corner to the bottom right dot. Fill with initials
                            if(dotGameBoard.playerTurn == 1)
                            {
                                dotGameBoard.p1Score += 1;
                                [dotGameBoard drawInitials:upperLeftDot width:(lowerRightDot.x - upperLeftDot.x) height:(lowerRightDot.y - upperLeftDot.y) player:1 andBoard:dotGameBoard];
                            }
                            else 
                            {
                                dotGameBoard.p2Score += 1;
                                [dotGameBoard drawInitials:upperLeftDot width:(lowerRightDot.x - upperLeftDot.x) height:(lowerRightDot.y - upperLeftDot.y) player:2 andBoard:dotGameBoard];
                            }
                            
                            
                            anotherTurn = 1;
                            
                        }
                        
                    }
                    
                }
            }
            if(passedDot.right == 1) // there must be a vertical line just drawn closing a box to it's right
            {
                if(GAMELOGICPRINT)
                    NSLog(@"\nBOX RIGHT: Starting Box right\n");
                 helper = previousMove / (dotGameBoard.yDimension -1);
                helper2 = helper * (dotGameBoard.xDimension-1);
                helper3 = previousMove % (dotGameBoard.yDimension-1);
                helper4 = ((helper3 - 1) + helper2) + 1;
                topLine = helper4;
                NSNumber *temp = [dotGameBoard.xLineArr objectAtIndex:helper4];
                if([temp intValue]) // checking to see if top right line connected
                {
                    if(GAMELOGICPRINT)
                        NSLog(@"\nBOX RIGHT: Top right line connected\n");
                     helper = previousMove + 1;
                    temp = [dotGameBoard.yLineArr objectAtIndex:helper];
                    if([temp intValue]) //checking to see if far right line connected
                    {
                        if(GAMELOGICPRINT)
                            NSLog(@"\nBOX RIGHT: Far right line connected\n");
                         helper = (previousMove) / (dotGameBoard.yDimension-1);
                        helper2 = (helper +1) * (dotGameBoard.xDimension-1);
                        helper3 = previousMove % (dotGameBoard.yDimension-1);
                        helper4 = ((helper3 -1) + helper2) +1;
                        temp = [dotGameBoard.xLineArr objectAtIndex:helper4];
                        if([temp intValue])
                        {
                            helper = topLine/(dotGameBoard.xDimension-1);
                            helper2 = helper * dotGameBoard.xDimension;
                            helper3 = topLine % (dotGameBoard.xDimension-1);
                            helper4 = helper2 + helper3;
                            if(GAMELOGICPRINT)
                                NSLog(@"\n\n INDEX = %d\n\n", helper4);
                             upperLeftDot = [[dotGameBoard.dotArr objectAtIndex:helper4]dotLocation];
                            lowerRightDot = [[dotGameBoard.dotArr objectAtIndex:(helper4 + dotGameBoard.xDimension+1)]dotLocation];
                            lowerRightDot = [[dotGameBoard.dotArr objectAtIndex:(helper4 + dotGameBoard.xDimension+1)]dotLocation];
                            
                            CGRect a = CGRectMake(upperLeftDot.x, upperLeftDot.y, (lowerRightDot.x - upperLeftDot.x), (lowerRightDot.y - upperLeftDot.y));
                            NSNumber *turnLabel;
                            if(dotGameBoard.playerTurn == 1)
                                turnLabel = [[NSNumber alloc]initWithInt:1];
                            else
                                turnLabel = [[NSNumber alloc]initWithInt:2];
                            
                            NSValue *pointObj = [NSValue valueWithCGRect:a];
                            [dotGameBoard.arrOfLabels replaceObjectAtIndex:topLine withObject:pointObj];
                            [dotGameBoard.arrHelper replaceObjectAtIndex:topLine withObject:turnLabel];
                            
                            if(GAMELOGICPRINT)
                                NSLog(@"\n\n BOX RIGHT: A BOX WAS JUST CONNECTED!!\n\n");
                             //there is a box connected just right of the given line just connected
                            //figure out the top right dot in this box, find a way to create
                            //a UILabel from that corner to the bottom right dot. Fill with initials
                            if(dotGameBoard.playerTurn == 1)
                            {
                                dotGameBoard.p1Score += 1;
                                [dotGameBoard drawInitials:upperLeftDot width:(lowerRightDot.x - upperLeftDot.x) height:(lowerRightDot.y - upperLeftDot.y) player:1 andBoard:dotGameBoard];
                            }
                            else 
                            {
                                dotGameBoard.p2Score += 1;
                                [dotGameBoard drawInitials:upperLeftDot width:(lowerRightDot.x - upperLeftDot.x) height:(lowerRightDot.y - upperLeftDot.y) player:2 andBoard:dotGameBoard];
                            }
                            
                            anotherTurn = 1;
                            
                        }
                    }
                    
                }
                
            }
        }
    }
    //change turns if necessary. If anotherTurn flag has been set
    //don't switch turns, current player just scored and should go again.
    /*figure out an elegant way to show who's turn it is
     * 1. Making player name bold or underlined
     * 2. Putting a dot next to their name
     * 3. Some other custom image. 
     */
    if(GAMELOGICPRINT)
        NSLog(@"dotGameBoard.playerTurn == %d\n", dotGameBoard.playerTurn);
     
    if((dotGameBoard.playerTurn == 1) && (anotherTurn))
    {
        if(GAMELOGICPRINT)
            NSLog(@"\nsending change in p1 score = %d\n", dotGameBoard.p1Score);
         [[NSNotificationCenter defaultCenter]
         postNotificationName:@"updateScoreP1"
         object:dotGameBoard];
    }else if((dotGameBoard.playerTurn == 2) && (anotherTurn))
    {
        if(GAMELOGICPRINT)
            NSLog(@"\nsending change in p2 score = %d\n", dotGameBoard.p2Score);
         [[NSNotificationCenter defaultCenter]
         postNotificationName:@"updateScoreP2"
         object:dotGameBoard];
    }
    
    
    NSString *winnerName;
    NSString *winnertitle;
    NSString *winnerdesc;
    int gameover = 0;
    int p1win = 0;
    int p2win = 0;

    if(((dotGameBoard.p1Score + dotGameBoard.p2Score) == ((dotGameBoard.xDimension-1) * (dotGameBoard.yDimension-1) )) || p1win || p2win)
    {
        gameover = 1;
        if((dotGameBoard.p2Score > dotGameBoard.p1Score) || p2win)
        {
            winnerName = dotGameBoard.player2; 
            winnertitle = [NSString stringWithFormat:@"%@", winnerName];
            winnerdesc = [NSString stringWithFormat:@"\nCongrats %@, You beat %@!", winnerName, dotGameBoard.player1];
            dotGameBoard.playerTurn = 1;
            
        }
        if((dotGameBoard.p1Score > dotGameBoard.p2Score) || p1win)
        {
            winnerName = dotGameBoard.player1;
            winnertitle = [NSString stringWithFormat:@"%@", winnerName];
            winnerdesc = [NSString stringWithFormat:@"\nCongrats %@, You beat %@!", winnerName, dotGameBoard.player2];
            dotGameBoard.playerTurn = 2;
            
            
        }
        if( dotGameBoard.p1Score == dotGameBoard.p2Score)
        {
            winnertitle = [NSString stringWithFormat:@"Tie Game"];
            winnerdesc = [NSString stringWithFormat:@"\nLooks like it was a tie"];
        }
    }
    if(gameover)
    {
        NSMutableArray *arr = [[NSMutableArray alloc] initWithObjects: winnertitle, winnerdesc, nil];
        //[[NSNotificationCenter defaultCenter]
        // postNotificationName:@"gameEnded"
        // object:arr];
        return arr;
    }
    if(!anotherTurn)
    {
        if(!illegalmove)
        {
            if(dotGameBoard.playerTurn == 1)
                dotGameBoard.playerTurn = 2;
            else
                dotGameBoard.playerTurn = 1;
        }
    }
    
    anotherTurn = 0;
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"changeTurn"
     object:dotGameBoard];
    return nil;
    
}

/**************************************************************************************
 * Purpose: given the CGPoint corresponding to the upper left dot of the box that was 
 * just won, also though of as the upper left corner of the box that was just won. 
 * Given the starting location of the box, height, width, player and gameboard it acts
 * as a wrapper to send a mutable array to the CustomUIView class to draw the initial
 * onto the gameboard. Calling drawInitials sends that NSMutableArray to the custom
 * UIView, which then gets drawn to board.
 *
 * Reference: Function called by lineLogic, above when it is found that a player has  
 * just won a box, wraps that data into a NSMutable array and sends that data via
 * notification to the customUIView to be draw onto the gameboard.
 **************************************************************************************/

-(void)drawInitials:(CGPoint)start width:(int)w height:(int)h player:(int)p andBoard:(gameLogic *)g
{
    if(GAMELOGICPRINT)
        NSLog(@"floatx = %f  floaty = %f",start.x, start.y);
     NSNumber *startx = [[NSNumber alloc]initWithFloat:start.x];
    NSNumber *starty = [[NSNumber alloc]initWithFloat:start.y];
    NSNumber *width = [[NSNumber alloc]initWithInt:w];
    NSNumber *height = [[NSNumber alloc]initWithInt:h];
    NSString *playerInitials;
    
    if(p == 1)
        playerInitials = g.p1Initials;
    else
        playerInitials = g.p2Initials;
    
    NSMutableArray *arr = [[NSMutableArray alloc]initWithObjects:startx, starty, width, height, playerInitials, nil];
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"drawInitials"
     object:arr];
}


/***************************************************************************************
* Purpose: Calculates the line to be drawn depending on what dot the initial touch was
* closest to. It will calculate the dot closest and depending on the touches orientation
* it will draw a line to the closest dot, making gameplay quite a bit easier.
*
* Refrence: Called by Custom UIView when a touches began event is registered, passes in
* a CGPoint of the initial touch and returns by passing the firstTemp dot object to the
* lineSnappingAndDrawing function declared above, by sending it a notification.
**************************************************************************************/
-(void)calculateClosestDots:(NSNotification *)notification
{
    [self setInitialMove:1];
    CGPoint val = [[notification object]CGPointValue];
    Dot* temp;
    Dot* closest;
    Dot *firstTemp;
    Dot *secondTemp;
    int flagError = 0;
    int xmargin = 120;
    int ymargin = 120;
    int testx, testy;
    for(int i = 0; i < (self.xDimension * self.yDimension); i++)
    {
        
        temp = [self.dotArr objectAtIndex:i];
        testx = abs(temp.dotLocation.x - val.x);
        testy = abs(temp.dotLocation.y - val.y);
        if(testx <= xmargin && testy <= ymargin)
        {
            xmargin = testx;
            ymargin = testy;
            closest = temp;
        }
    }
    if(GAMELOGICPRINT)
        NSLog(@"\nCLOSEST = %d\n", [closest dotID]);
 if(abs(closest.dotLocation.x - val.x) > abs(closest.dotLocation.y - val.y))
    {
        if (closest.dotLocation.x - val.x < 0) //right
        {
            if(closest.right)
            {
                firstTemp = closest;
                secondTemp = [self.dotArr objectAtIndex:closest.dotID + 1];
            }
            else
                flagError = 1;
        }
        else if(closest.dotLocation.x - val.x >= 0) // left
        {
            if(closest.left)
            {
                firstTemp = closest;
                secondTemp = [self.dotArr objectAtIndex:closest.dotID - 1];
            }
            else
                flagError = 1;
        }
    }
    else if(abs(closest.dotLocation.x - val.x) < abs(closest.dotLocation.y - val.y))
     {
        if(closest.dotLocation.y - val.y >= 0) //up
        {
            if(closest.up)
            {   
                firstTemp = closest;
                secondTemp = [self.dotArr objectAtIndex:(closest.dotID - self.xDimension)];
            }
            else
                flagError = 1;
        }
        else if(closest.dotLocation.y - val.y < 0) //down
        {
            if(closest.down)
            {
                firstTemp = closest;
                secondTemp = [self.dotArr objectAtIndex:(closest.dotID + self.xDimension)];
            }
            else
                flagError = 1;
        }
    }
    if(flagError == 0)
    {
        firstTemp.endLocation = secondTemp.dotLocation;
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"touchesEndedForDot"
         object:firstTemp];
    }
}


/**************************************************************************************
 * Purpose: When a game is ended the data for that instance of the game is set back to
 * nil, or zero. Making sure that the previous game data is removed. If option flag is
 * set to 0 erase everything, if not don't erase player names.
 *
 * Reference: Called in gameBoardViewController when a game is quit or when a game is 
 * won or lost and the user is prompted to play another game which is in the lineLogic 
 * function above.
 *
 **************************************************************************************/

-(void)resetGame:(gameLogic *)gameBoard option:(int)option
{
    if(!option)
    {
        [gameBoard setPlayer1:nil];
        [gameBoard setPlayer2:nil];
    }
    if(!gameBoard.playAgain)
        gameBoard.playerTurn = 1;
    [gameBoard setP1Initials:nil];
    [gameBoard setP2Initials:nil];
    [gameBoard setDotArr:nil];
    [gameBoard setXLineArr:nil];
    [gameBoard setYLineArr:nil];
    [gameBoard setDot:nil];
    [gameBoard setInitialTouch:nil];
    [gameBoard setFirstTouch:CGPointZero];
    [gameBoard setFinalTouch:nil];
    [gameBoard setAlreadyStartedGame:0];
    [gameBoard setP1Score:1000];
    [gameBoard setP2Score:0];
    [gameBoard setArrOfLabels:nil];
    [gameBoard setArrHelper:nil];
    [gameBoard setLastTouch:CGPointZero];
    [gameBoard setUpdateLineArray:-1];
    [gameBoard setIndexToUpdateWith:0];
    [gameBoard setPassedDotLocation:CGPointZero];
    [gameBoard setLineLogicDot:nil];
    [gameBoard setReload:0];
    [gameBoard setBoxbotflag:0];
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"eraseData"
     object:nil];
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"removeNotifications"
     object:nil];
    
}

-(int)assignGameDataID:(gameLogic *)gb
{
    NSString *moifile;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *docs = [paths  objectAtIndex:0];
    NSFileManager *fm = [[NSFileManager alloc]init];

    NSString *myFolder;
    
    if(gb.onlineGame == 0)
        myFolder = [docs stringByAppendingFormat:@"/12Dot/Local"];
    else if(gb.onlineGame == 1)
        myFolder = [docs stringByAppendingFormat:@"/12Dot/Online"];
    
    NSString *str = @"gameData";
    NSString *strPath;
    for(int i = 1; i < 11; i++)
    {
        strPath = [str stringByAppendingString:[NSString stringWithFormat:@"%d", i]];
        moifile = [myFolder stringByAppendingPathComponent:strPath];
        if(![fm fileExistsAtPath:moifile])
        {
            gb.gameDataID = i;
            return 1;
        }
    }
    
    return -1;
}

@end
