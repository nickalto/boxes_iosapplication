//
//  secondViewController.h
//  Boxes_iOSApplication
//
//  Created by NICK ALTO on 1/30/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "gameBoardViewController.h"
#import <QuartzCore/QuartzCore.h> 
#import "gameData.h"

@interface secondViewController : UIViewController
{
    
    gameLogic *gameBoard;                       // gameboard instance
    UIGestureRecognizer *gestureRecognizer;     // gestureRecognizer
    IBOutlet UITextField *p1label;              // text area for first player to enter name
    IBOutlet UITextField *p2label;              // text are for second player to enter name
    IBOutlet UIButton *beginGame;               // button to start game once all data is entered
    IBOutlet UIButton *fiveXSix;                // grid size determining size of boxes layout
    IBOutlet UIButton *sixXSeven;
    gameBoardViewController *gameController;    // next view controller
    gameData *gData;
    IBOutlet UIButton *player1Check;
    IBOutlet UIButton *player2Check;
    IBOutlet UIButton *p1OptionButton;
    IBOutlet UIButton *p2OptionButton;
    IBOutlet UIImageView *textfield1;
    IBOutlet UIImageView *textfield2;

}

//Synthesized Variables
@property(nonatomic, strong) gameLogic *gameBoard;
@property(nonatomic, strong) gameData *gData;

//Public Instance Methods
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andBoard:(id)gameBoard andData:(id)g;


@end
