//
//  restoreGameViewController.m
//  Boxes_iOSApplication
//
//  Created by NICK ALTO on 4/14/12.
//  Copyright (c) 2012 Oregon State University. All rights reserved.
//

#import "restoreGameViewController.h"

@interface restoreGameViewController ()
{
    int secondData;
    UIColor *textColor;
    BlockAlertView *alert;
    CATransition *animation;
    UIBarButtonItem *forwardBarButtonItem;
}
@end

@implementation restoreGameViewController
@synthesize gameBoard           = _gameBoard;
@synthesize gData               = _gData;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andBoard:(gameLogic *)game andData:(gameData *)data ID:(int)passedID
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        gameBoard               = game;
        gData                   = data;
        controllerID            = passedID;
        secondData              = 0;
        if(IS_IPHONE5)
            [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"i5.png"]]];
        else
            [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"plainBackground.jpg"]]];        
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated
{

    UINavigationBar *navBar = self.navigationController.navigationBar;
    [navBar clearsContextBeforeDrawing];
    UIFont *labelFont = [UIFont fontWithName:@"FFFTusj-Bold" size:18];
    [navBar setBackgroundImage:[UIImage imageNamed:@"header.png"] forBarMetrics:UIBarMetricsDefault];
    animation = [CATransition animation];
    UIImage *forwardImage = [UIImage imageNamed:@"forwardIconBrown.png"];
    UIButton *forwardButton = [UIButton buttonWithType:UIButtonTypeCustom];
    forwardButton.frame = CGRectMake(280, 0, 40, 40);
    
    [forwardButton setImage:forwardImage forState:UIControlStateNormal];
    
    forwardBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:forwardButton];
    
    UIImage *backImage = [UIImage imageNamed:@"backIconBrown.png"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 0, 40, 40);
    
    [backButton setImage:backImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    [forwardButton addTarget:self action:@selector(nextScreen:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = backBarButtonItem;

    self.view.frame = CGRectMake(0.0, 20.0, 320.0, 460.0);
    self.view.alpha = 1.000;
    int currID;
    if(controllerID == 1)
        currID = 0;
    else if(controllerID == 2)
        currID = 5;

    for(int i = 0; i<5; i++, currID++)
    {
        int offset = 85.0;
        int p1offset = 28.0;
        int p2offset = 58.0;

        UIButton *game = [UIButton buttonWithType:UIButtonTypeCustom];
        game.adjustsImageWhenDisabled = YES;
        game.adjustsImageWhenHighlighted = YES;
        game.autoresizesSubviews = YES;
        game.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
        game.clearsContextBeforeDrawing = YES;
        game.clipsToBounds = NO;
        game.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        game.contentMode = UIViewContentModeScaleToFill;
        game.contentStretch = CGRectFromString(@"{{0, 0}, {1, 1}}");
        game.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        game.enabled = YES;
        game.frame = CGRectMake(2.0, 15.0 + (i * offset), 273.0, 90.0);
        game.tag = currID+1;
        game.userInteractionEnabled = YES;
        [game setBackgroundImage:[UIImage imageNamed:@"RGbutton1.png"]forState:UIControlStateNormal];
        [game setBackgroundImage:[UIImage imageNamed:@"RGbutton1Selected.png"]forState:UIControlStateHighlighted];
        [game addTarget:self action:@selector(gamePressed:) forControlEvents:UIControlEventTouchUpInside];

        [self.view addSubview:game];
        
        UIButton *gameDel = [UIButton buttonWithType:UIButtonTypeCustom];
        gameDel.adjustsImageWhenDisabled = YES;
        gameDel.adjustsImageWhenHighlighted = YES;
        gameDel.autoresizesSubviews = YES;
        gameDel.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
        gameDel.clearsContextBeforeDrawing = YES;
        gameDel.clipsToBounds = NO;
        gameDel.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        gameDel.contentMode = UIViewContentModeScaleToFill;
        gameDel.contentStretch = CGRectFromString(@"{{0, 0}, {1, 1}}");
        gameDel.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        gameDel.enabled = YES;
        gameDel.frame = CGRectMake(275.0, 15.0 + (i * offset), 43.0, 90.0);
        gameDel.tag = currID+1;
        gameDel.userInteractionEnabled = YES;
        [gameDel.titleLabel setFont:labelFont];
        [gameDel.titleLabel setTextColor:textColor];
        [gameDel setBackgroundImage:[UIImage imageNamed:@"RGbutton1Del.png"]forState:UIControlStateNormal];
        [gameDel setBackgroundImage:[UIImage imageNamed:@"RGbutton1DelSelected.png"]forState:UIControlStateHighlighted];
        [gameDel addTarget:self action:@selector(gameDelete:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:gameDel];

        
        UILabel *p1label = [[UILabel alloc] initWithFrame:CGRectMake(20.0, 28.0, 140.0, 30.0)];
        p1label.adjustsFontSizeToFitWidth = YES;
        p1label.autoresizesSubviews = YES;
        p1label.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
        p1label.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
        p1label.clearsContextBeforeDrawing = YES;
        p1label.clipsToBounds = YES;
        p1label.contentMode = UIViewContentModeCenter;
        p1label.contentStretch = CGRectFromString(@"{{0, 0}, {1, 1}}");
        p1label.frame = CGRectMake(20.0, p1offset + (i * offset), 140.0, 30.0);
        p1label.lineBreakMode = UILineBreakModeTailTruncation;
        p1label.textAlignment = UITextAlignmentLeft;
        p1label.backgroundColor = [UIColor clearColor];
        p1label.textColor = textColor;
        p1label.font = labelFont;
        p1label.tag = currID+1;
        [self.view addSubview:p1label];
        
        UILabel *p2label = [[UILabel alloc] initWithFrame:CGRectMake(20.0, 28.0, 140.0, 30.0)];
        p2label.adjustsFontSizeToFitWidth = YES;
        p2label.autoresizesSubviews = YES;
        p2label.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
        p2label.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
        p2label.clearsContextBeforeDrawing = YES;
        p2label.clipsToBounds = YES;
        p2label.contentMode = UIViewContentModeCenter;
        p2label.contentStretch = CGRectFromString(@"{{0, 0}, {1, 1}}");
        p2label.frame = CGRectMake(20.0, p2offset + (i * offset), 140.0, 30.0);
        p2label.lineBreakMode = UILineBreakModeTailTruncation;
        p2label.textAlignment = UITextAlignmentLeft;
        p2label.backgroundColor = [UIColor clearColor];
        p2label.textColor = textColor;
        p2label.font = labelFont;
        p2label.tag = currID+1;
        [self.view addSubview:p2label];
        
        UILabel *p1score = [[UILabel alloc] initWithFrame:CGRectMake(20.0, 28.0, 140.0, 30.0)];
        p1score.adjustsFontSizeToFitWidth = YES;
        p1score.autoresizesSubviews = YES;
        p1score.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
        p1score.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
        p1score.clearsContextBeforeDrawing = YES;
        p1score.clipsToBounds = YES;
        p1score.contentMode = UIViewContentModeCenter;
        p1score.contentStretch = CGRectFromString(@"{{0, 0}, {1, 1}}");
        p1score.frame = CGRectMake(158.0, p1offset + (i * offset), 34.0, 30.0);
        p1score.lineBreakMode = UILineBreakModeTailTruncation;
        p1score.textAlignment = UITextAlignmentLeft;
        p1score.backgroundColor = [UIColor clearColor];
        p1score.textColor = textColor;
        p1score.font = labelFont;
        p1score.tag = currID+1;
        [self.view addSubview:p1score];
        
        UILabel *p2score = [[UILabel alloc] initWithFrame:CGRectMake(20.0, 28.0, 140.0, 30.0)];
        p2score.adjustsFontSizeToFitWidth = YES;
        p2score.autoresizesSubviews = YES;
        p2score.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
        p2score.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
        p2score.clearsContextBeforeDrawing = YES;
        p2score.clipsToBounds = YES;
        p2score.contentMode = UIViewContentModeCenter;
        p2score.contentStretch = CGRectFromString(@"{{0, 0}, {1, 1}}");
        p2score.frame = CGRectMake(158.0, p2offset + (i * offset), 34.0, 30.0);
        p2score.lineBreakMode = UILineBreakModeTailTruncation;
        p2score.textAlignment = UITextAlignmentLeft;
        p2score.backgroundColor = [UIColor clearColor];
        p2score.textColor = textColor;
        p2score.font = labelFont;
        p2score.tag = currID+1;
        [self.view addSubview:p2score];
        
        UILabel *percent = [[UILabel alloc] initWithFrame:CGRectMake(20.0, 28.0, 140.0, 30.0)];
        percent.adjustsFontSizeToFitWidth = YES;
        percent.autoresizesSubviews = YES;
        percent.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleBottomMargin;
        percent.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
        percent.clearsContextBeforeDrawing = YES;
        percent.clipsToBounds = YES;
        percent.contentMode = UIViewContentModeCenter;
        percent.contentStretch = CGRectFromString(@"{{0, 0}, {1, 1}}");
        percent.frame = CGRectMake(228.0, p2offset + (i * offset), 34.0, 30.0);
        percent.lineBreakMode = UILineBreakModeTailTruncation;
        percent.textAlignment = UITextAlignmentLeft;
        percent.backgroundColor = [UIColor clearColor];
        percent.textColor = textColor;
        percent.font = labelFont;
        percent.tag = currID+1;
        [self.view addSubview:percent];
        
        p1label.textColor = p2label.textColor = p1score.textColor = p2score.textColor = percent.textColor =
        [UIColor colorWithRed:.2509 green:.2157 blue:.1843 alpha:1];

        gameData *tempgame;
        tempgame = [self getGame:currID+1 del:0];
        
        if(tempgame)
        {
            p1label.text = [NSString stringWithFormat:@" %@ ", tempgame.p1];
            p2label.text = [NSString stringWithFormat:@" %@ ", tempgame.p2];;
            p1score.text = [NSString stringWithFormat:@" %d ",[tempgame.p1score intValue]];
            p2score.text = [NSString stringWithFormat:@" %d ",[tempgame.p2score intValue]];
            NSString* str = [NSString stringWithFormat:@" %d ", [self calculateGamePercentage:tempgame]];
            str = [str stringByAppendingString:@"%"];
            percent.text = str;
        }
        else {
            p1label.text = @"";
            p2label.text = @"";
            p1score.text = @"";
            p2score.text = @"";
            percent.text = @"";
        }

    }
        
    [super viewWillAppear:animated];
    // Do any additional setup after loading the view from its nib.
}

-(gameData *)getGame:(int)i del:(int)delete
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString* docs = [paths  objectAtIndex:0];
    NSFileManager *fm = [[NSFileManager alloc]init];
    
    NSString *myFolder = [docs stringByAppendingFormat:@"/12Dot/Local"];
    if( gameBoard.onlineGame ) {
        myFolder = [docs stringByAppendingFormat:@"/12Dot/Online"];
    }
    NSString *moifile = [myFolder stringByAppendingPathComponent:[NSString stringWithFormat:@"gameData%d", i]];
    
    BOOL exists = [fm fileExistsAtPath:moifile];
    gameData *tempgame = NULL;
    if(exists)
    {
        NSData *data = [[NSData alloc]initWithContentsOfFile:moifile];
        tempgame = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        if(delete)
        {
            NSError *err;
            [fm removeItemAtPath:moifile error:&err];
            
            for(UILabel *label in self.view.subviews) {
                if(label.tag == i && [label isKindOfClass:[UILabel class]])
                    label.text = @"";
            }
        }

    }
    if(!secondData && controllerID != 2)
    {
        for(int k = 6; i<12; i++)
        {
            moifile = [myFolder stringByAppendingPathComponent:[NSString stringWithFormat:@"gameData%d", k]];
            exists = [fm fileExistsAtPath:moifile];
            if(exists)
                self.navigationItem.rightBarButtonItem = forwardBarButtonItem;
        }
    }
   
    return tempgame;
}


- (IBAction) gamePressed :(id)sender
{
    UIButton *temp = (UIButton *)sender;
    gameData *pressedGame = [self getGame:temp.tag del:0];

    if(pressedGame != NULL)
    {
        int xDim = [pressedGame.xDim intValue];
        int yDim = [pressedGame.yDim intValue];
        gameBoard.player1 = pressedGame.p1;
        gameBoard.p1Score = [pressedGame.p1score intValue];
        gameBoard.p1Initials = [NSMutableString stringWithFormat:@"%@", pressedGame.p1initials];
        gameBoard.player2 = pressedGame.p2;
        gameBoard.p2Score = [pressedGame.p2score intValue];
        gameBoard.p2Initials = [NSMutableString stringWithFormat:@"%@", pressedGame.p2initials];
        gameBoard.playerTurn = [pressedGame.turn intValue];
        gameBoard.xLineArr = pressedGame.xLineArray;
        gameBoard.yLineArr = pressedGame.yLineArray;
        gameBoard.arrOfLabels = pressedGame.initialsCGRect;
        gameBoard.arrHelper = pressedGame.initialsPlayer;
        gameBoard.xDimension = xDim;
        gameBoard.yDimension = yDim;
        gameBoard.boxbotflag = [pressedGame.boxbotflag intValue];
        gameBoard.reload = 1;
        gameBoard.gameDataID = [pressedGame.gameDataID intValue];
        
        gameController = [[gameBoardViewController alloc]initWithNibName:@"gameBoardViewController" bundle:nil andBoard:gameBoard andData:pressedGame];
        
        [self.navigationController setToolbarHidden:NO];
        [self.navigationController setNavigationBarHidden:FALSE];
        [self.navigationController.toolbar setTintColor:[UIColor whiteColor]];
        [self animations];
    }
}

-(void)animations
{
    [animation setDelegate:self];
    [animation setDuration:0.6];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    animation.type = @"pageCurl";
    
    animation.subtype = kCATransitionFromTop;
    animation.fillMode = kCAFillModeForwards;
    animation.startProgress = 0;
    [animation setRemovedOnCompletion:YES];
    
    [self.navigationController.view.layer addAnimation:animation forKey:@"pageCurlAnimation"];
    [self.navigationController pushViewController:gameController animated:NO];
}

-(IBAction)gameDelete:(id)sender
{
    UIButton *temp = sender;
    
    alert = [BlockAlertView alertWithTitle:@"Delete Game" message:@"\nAre you sure you want to delete this game?"];
    __weak restoreGameViewController *weakSelf = self;
    
    [alert setCancelButtonWithTitle:@"Yes" block:^{
        [weakSelf getGame:temp.tag del:1];
    }];
    [alert setDestructiveButtonWithTitle:@"No" block:nil];
    [alert show];
    
}

-(void)backButton:(id)sender
{
    [animation setDelegate:self];
    [animation setDuration:0.5];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    animation.type = @"pageUnCurl";
    
    animation.subtype = kCATransitionFromBottom;
    animation.fillMode = kCAFillModeBackwards;
    animation.startProgress = 0;
    [animation setRemovedOnCompletion:YES];
    
    [self.navigationController.view.layer addAnimation:animation forKey:@"pageCurlAnimation"];
    [self.navigationController popViewControllerAnimated:NO];
}


-(int)calculateGamePercentage:(gameData *)gameID
{
    gameData *temp = gameID;
    @try {

        float total = 0.0;
        float count = 0.0;
        int x = 0; 
        int y = 0;
        
        if([temp.xDim intValue] == 5)
        {
            total = 49.0;
            x = 24;
            y = 25;
        }
        else
        {
            total = 97.0;
            x = 48; 
            y = 49;
        }

        for(int i = 0; i < x; i++)
        {
            if([[temp.xLineArray objectAtIndex:i]intValue] == 1 )
                count++;
        }
        for(int j = 0; j < y; j++)
        {
            if([[temp.yLineArray objectAtIndex:j]intValue] == 1)
                count++;
        }
        
        int ret = (count/total * 100);
            
        
        return ret;
        }
    @catch (NSException * e) {
        [self getGame:[temp.gameDataID intValue] del:1];
            NSLog(@"Exception: %@", e);
    }
    
}

-(IBAction)nextScreen:(id)sender
{
    nextVC = [[restoreGameViewController alloc]initWithNibName:@"restoreGameViewController2" bundle:nil andBoard:gameBoard andData:gData ID:2];
    self.navigationController.view.autoresizesSubviews = FALSE;
    [animation setDelegate:self];
    [animation setDuration:0.6];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    animation.type = @"pageCurl";
    
    animation.subtype = kCATransitionFromTop;
    animation.fillMode = kCAFillModeForwards;
    animation.startProgress = 0;
    [animation setRemovedOnCompletion:YES];
    
    [self.navigationController.view.layer addAnimation:animation forKey:@"pageCurlAnimation"];
    [self.navigationController pushViewController:nextVC animated:NO];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
