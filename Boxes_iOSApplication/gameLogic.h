//
//  gameLogic.h
//  Boxes_iOSApplication
//
//  Created by NICK ALTO on 1/30/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Dot.h"

@interface gameLogic : NSObject 
{
    int gameDataID;
    NSString *player1;                  // Player 1's name
    NSString *player2;                  // Player 2's name
    NSMutableString *p1Initials;        // Initials for player 1
    NSMutableString *p2Initials;        // Initials for player 2
    int xDimension;                     // The xDimension size of gameboard
    int yDimension;                     // The yDimension size of gameboard
    NSMutableArray* xLineArr;           // Array of lines between dots (y axis)
    NSMutableArray* yLineArr;           // Array of lines between dots (x axis)
    NSMutableArray* dotArr;             // Array for dots depending on size of board
    Dot* dot;                           // Dot object 
    Dot* initalTouch;                   // To keep track fo the first dot touched
    Dot* finalTouch;                    // To keep track of the last dot touched
    BOOL alreadyStartedGame;            // 0 if no game is started already 1 if it is
    int p1Score;                        // Player 1 score
    int p2Score;                        // Player 2 score
    int boxbotflag;
    NSMutableArray *arrOfLabels;        // corresponds to a cgrect of a box won by player
    NSMutableArray *arrHelper;          // array corresponding to player that has won the box in arrOfLables
    int playerTurn;                     // self explanatory;
    CGPoint firstTouch;                 // first dot touched used in lineLogic
    CGPoint lastTouch;                  // second/last dot touched used in lineLogic
    int updateLineArray;                //  0 for x, 1 for y
    int indexToUpdateWith;              // index in either xLineArray or yLineArray
    CGPoint passedDotLocation;          // CGRect corresponding to the passedDotLocation
    Dot* lineLogicDot;                  // Temp dot object
    int playAgain;                      // keep player names in second view controller if 1
    int reload;
    int refreshBoard;                   //refresh display when resuming from inactive state
    int onlineGame;                     // flag determining if online game was specified 
    int initialMove;                    // solve bug causing turn to change when no move was made
}

//Synthisized Variables 
@property (nonatomic, strong)    NSMutableArray *arrHelper; 
@property (nonatomic, strong)    NSMutableArray *arrOfLabels; 
@property (nonatomic, strong) Dot *lineLogicDot;
@property CGPoint passedDotLocation;
@property int updateLineArray;
@property int indexToUpdateWith;
@property int boxbotflag;
@property CGPoint firstTouch;
@property CGPoint lastTouch;
@property (nonatomic, strong) NSMutableArray *dotArr;
@property (nonatomic, strong) NSMutableArray *xLineArr;
@property (nonatomic, strong) NSMutableArray *yLineArr;
@property (nonatomic, strong) Dot *initialTouch;
@property (nonatomic, strong) Dot *finalTouch;
@property (nonatomic, strong) Dot *dot;
@property (nonatomic, strong) NSString *player1;
@property (nonatomic, strong) NSString *player2;
@property (nonatomic, strong) NSMutableString *p1Initials;
@property (nonatomic, strong) NSMutableString *p2Initials;
@property int playerTurn;
@property int xDimension;
@property int yDimension;
@property int p1Score;
@property int p2Score;
@property BOOL alreadyStartedGame;
@property int playAgain;
@property int reload;
@property int refreshBoard;
@property int onlineGame;
@property int gameDataID;
@property int initialMove;

//Instance Methods
- (NSMutableArray *)setUpDots:(gameLogic *)gb;
- (NSMutableArray *)lineLogic:(gameLogic *)dotGameBoard;
- (void)drawInitials:(CGPoint)start width:(int)w height:(int)h player:(int)p andBoard:(gameLogic *)gameBoard;
- (void)resetGame:(gameLogic *)gameBoard option:(int)option;
- (int)assignGameDataID:(gameLogic *)gb;
- (void) AI:(gameLogic *)game;

@end
