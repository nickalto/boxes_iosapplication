//
//  Dot.m
//  Boxes_iOSApplication
//
//  Created by NICK ALTO on 1/30/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "Dot.h"

@implementation Dot

@synthesize dotID           = _dotID;
@synthesize dotLocation     = _dotLocation;
@synthesize up              = _up;
@synthesize down            = _down;
@synthesize right           = _right;
@synthesize left            = _left;
@synthesize gameBoard       = _gameBoard;
@synthesize endLocation     = _endLocation;
@synthesize xDim            = _xDim;
@synthesize dotButton       = _dotButton;


/**************************************************************************************
 * Purpose: Initializes Dot object setting the background and setting synth'd variables
 * to 0, while also setting the initial image for the dot object/custom button.
 *
 * Reference: Called from gameLogic when the dot array is being initialized. 
 **************************************************************************************/

- (id)initWithID:(int)did 
{
    self = [super init];
    if (self) {
        self.dotButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.dotButton setBackgroundImage:[UIImage imageNamed:@"dot.png"] forState:UIControlStateNormal];
        self.dotButton.userInteractionEnabled = TRUE;
        [self.dotButton setBackgroundImage:[UIImage imageNamed:@"dotTouched.png"] forState:UIControlEventTouchDown];
        //[self.dotButton addTarget:self action:@selector(touched:) forControlEvents:UIControlEventTouchDown];

        [self setDotID:did];
        dotLocation.x   = 0;
        dotLocation.y   = 0;
        up              = 0;
        down            = 0;
        right           = 0;
        left            = 0;
    }
    
    return self;
}


@end
