//
//  customUIView.h
//  Boxes_iOSApplication
//
//  Created by NICK ALTO on 2/1/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface customUIView : UIView
{
    CGPoint first;                      // first Dot touched by user
    CGPoint second;                     // second Dot touched by user
    int saveContextAndCommitDrawing;    // Flag used to commit line drawn by user 
    int draw;                           // Flag used to draw line by user w/o commiting it
    int userApproved;                   // Additional flag used to commit line drawn by user
    NSMutableArray *locArr;             // Array of CGPoints corresponding to committed lines
    int numberFloats;                   // Count for how many lines are held in locArr

}

//Synthesized Variables
@property int numberFloats;
@property bool userApproved;
@property bool draw;
@property bool saveContextAndCommitDrawing;
@property CGPoint first;
@property CGPoint second;
@property(nonatomic, strong) NSMutableArray *locArr;

//Public Instance Methods
- (void)drawInitials:(NSMutableArray *)arr;
- (void)finalizedByUser;
- (void)touched:(NSNotification *)notification;
- (id)initWithFrame:(CGRect)frame and:(id)g;
- (void)redrawLines:(NSMutableArray *)xLineArr yLineArray:(NSMutableArray *)yLineArr AndDots:(NSMutableArray *)initialsArray xDim:(int)xDim yDim:(int)yDim;
- (void)redrawInitials:(NSMutableArray *)initialsArr helper:(NSMutableArray *)helperArr xDim:(int)xDim yDim:(int)yDim p1Name:(NSString *)p1 p2Name:(NSString*)p2;
-(void)drawInitials:(NSMutableArray *)arr;
@end
