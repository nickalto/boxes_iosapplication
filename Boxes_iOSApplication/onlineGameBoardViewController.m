//
//  onlineGameBoardViewController.m
//  Boxes_iOSApplication
//
//  Created by NICK ALTO on 1/31/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "onlineGameBoardViewController.h"

@interface onlineGameBoardViewController()
{
    int gameOver;                           // determines if the game has ended
    int notificationID;                     // determines notification ID
    NSString *moifile;
    NSString *docs;
    NSFileManager *fm;
    UIColor *playButton;
    UIFont *sFont;
    BlockAlertView *alert2;
    BlockAlertView *alert;
    NSString *gameOverMessage;
    CATransition *animation;
}
@end

@implementation onlineGameBoardViewController
@synthesize contentView     = _contentView;
@synthesize gameBoard       = _gameBoard;

BOOL OGBVCPRINT = FALSE;

/**************************************************************************************
 * Purpose: Initialize onlineGameBoardViewController as well as register necessary notificaions
 * and initialize label/arrHelper arrays, and set the necessary sizing/zooming for the 
 * given gameboard dimensions
 *
 * Reference: n/a
 **************************************************************************************/



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andBoard:(gameLogic *)game andData:(gameData *)gData
{
    data = gData;
    gameOver = 0;
    notificationID = 0;
    if(OGBVCPRINT)
        NSLog(@"onlineGameBoardViewController is being initialized\n\n");
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        gameBoard = game;
        if(!gameBoard.reload)
        {
            gameBoard.arrOfLabels = [[NSMutableArray alloc]initWithCapacity:((gameBoard.xDimension-1) * (gameBoard.yDimension-1))];
            gameBoard.arrHelper = [[NSMutableArray alloc]initWithCapacity:((gameBoard.xDimension-1) * (gameBoard.yDimension-1))];
            NSNumber *temp = [[NSNumber alloc]initWithBool:FALSE];
            for(int i = 0; i<((gameBoard.xDimension-1) *(gameBoard.yDimension-1)); i++)
            {
                [gameBoard.arrOfLabels addObject:temp];
                [gameBoard.arrHelper addObject:temp];
            }
        }
        CGRect rect;
        if(gameBoard.xDimension == 5)
        {
            rect = CGRectMake(0, 0, (self.view.frame.size.width +100 ), (self.view.frame.size.height +125 ));
            scrollView.minimumZoomScale = .76;
            
        }
        else if(gameBoard.xDimension == 7)
        {
            rect = CGRectMake(0, 0, (self.view.frame.size.width +200 ), (self.view.frame.size.height +275 ));
            scrollView.minimumZoomScale = .62;
            
        }
        
        //notification for customUINavController
        [[NSNotificationCenter defaultCenter]
         addObserver:self
         selector:@selector(drawInitials:)
         name:@"drawInitials"
         object:nil];
        
        [[NSNotificationCenter defaultCenter]
         addObserver:self
         selector:@selector(coldBoot:)
         name:@"coldBoot"
         object:nil];
        
        //notification for customUINavController
        [self.navigationItem setHidesBackButton:TRUE];
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"changeTitle"
         object:gameBoard];
        
        
        [[NSNotificationCenter defaultCenter]
         addObserver:self
         selector:@selector(changeToolBarImage:)
         name:@"touchesEndedForDot"
         object:nil];
        
        contentView = [[customUIView alloc]initWithFrame:rect and:game];
        contentView.backgroundColor = [UIColor whiteColor];
        
        scrollView.backgroundColor = [UIColor grayColor];
        scrollView.contentSize = contentView.frame.size;
        [scrollView setDelegate:self];
        scrollView.userInteractionEnabled = TRUE;
        scrollView.contentMode = (UIViewContentModeScaleAspectFit);
        [scrollView addSubview:contentView];
        scrollView.maximumZoomScale = 1.3;
        [self.view addSubview:scrollView];
        if(gameBoard.xDimension == 5)
            [contentView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"graphpapersmall.png"]]];
        else if(gameBoard.xDimension == 7){
            [contentView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"graphpaperlarge.png"]]];
        }
        self.navigationController.toolbar.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"footer.png"]];
        
        rightHome = [UIButton buttonWithType:UIButtonTypeCustom];
        leftHome = [UIButton buttonWithType:UIButtonTypeCustom];
        submitScore = [UIButton buttonWithType:UIButtonTypeCustom];
        sFont = [UIFont fontWithName:@"FFFTusj-Bold" size:24];
        
        int firstArrow, secondArrow, thirdArrow;
        if(gameBoard.xDimension == 5)
        {
            firstArrow = 410;
            secondArrow = 430;
            thirdArrow = 450;
        }
        else
        {
            firstArrow = 510;
            secondArrow = 530;
            thirdArrow = 550;
        }
        CGRect arrFrame = CGRectMake(5, firstArrow, 20, 20);
        UILabel *leftArrow = [[UILabel alloc]initWithFrame:arrFrame];
        leftArrow.text = @">";
        leftArrow.font = sFont;
        leftArrow.transform = CGAffineTransformMakeRotation(1.57);
        leftArrow.textColor = [UIColor blackColor];
        leftArrow.backgroundColor = [UIColor clearColor];
        [contentView addSubview:leftArrow];
        
        CGRect arrFrame1 = CGRectMake(5, secondArrow, 20, 20);
        
        UILabel *leftArrow1 = [[UILabel alloc]initWithFrame:arrFrame1];
        leftArrow1.text = @">";
        leftArrow1.font = sFont;
        leftArrow1.transform = CGAffineTransformMakeRotation(1.57);
        leftArrow1.textColor = [UIColor blackColor];
        leftArrow1.backgroundColor = [UIColor clearColor];
        [contentView addSubview:leftArrow1];
        
        CGRect arrFrame2 = CGRectMake(5, thirdArrow, 20, 20);
        
        UILabel *leftArrow2 = [[UILabel alloc]initWithFrame:arrFrame2];
        leftArrow2.text = @">";
        leftArrow2.font = sFont;
        leftArrow2.transform = CGAffineTransformMakeRotation(1.57);
        leftArrow2.textColor = [UIColor blackColor];
        leftArrow2.backgroundColor = [UIColor clearColor];
        [contentView addSubview:leftArrow2];
        
        
        int width = rect.size.width - 20;
        CGRect arrLeftFrame = CGRectMake(width, firstArrow, 20, 20);
        UILabel *rightArrow = [[UILabel alloc]initWithFrame:arrLeftFrame];
        rightArrow.text = @">";
        rightArrow.font = sFont;
        rightArrow.transform = CGAffineTransformMakeRotation(1.57);
        rightArrow.textColor = [UIColor blackColor];
        rightArrow.backgroundColor = [UIColor clearColor];
        [contentView addSubview:rightArrow];
        
        CGRect arrLeftFrame1 = CGRectMake(width, secondArrow, 20, 20);
        UILabel *rightArrow1 = [[UILabel alloc]initWithFrame:arrLeftFrame1];
        rightArrow1.text = @">";
        rightArrow1.font = sFont;
        rightArrow1.transform = CGAffineTransformMakeRotation(1.57);
        rightArrow1.textColor = [UIColor blackColor];
        rightArrow1.backgroundColor = [UIColor clearColor];
        [contentView addSubview:rightArrow1];
        
        CGRect arrLeftFrame2 = CGRectMake(width, thirdArrow, 20, 20);
        UILabel *rightArrow2 = [[UILabel alloc]initWithFrame:arrLeftFrame2];
        rightArrow2.text = @">";
        rightArrow2.font = sFont;
        rightArrow2.transform = CGAffineTransformMakeRotation(1.57);
        rightArrow2.textColor = [UIColor blackColor];
        rightArrow2.backgroundColor = [UIColor clearColor];
        [contentView addSubview:rightArrow2];
        
    }
    return self;
}

/**************************************************************************************
 * Purpose: Initialize lines is called when game is reloaded from memory to redraw the
 * lines and initials drawn previously. Also called when passing game data back and 
 * forth from gamecenter games.
 *
 * Reference: n/a
 **************************************************************************************/

- (void)initializeLines:(gameLogic *)game andFlag:(BOOL)flag
{
    // NSMutableArray *arr;
    NSMutableArray *dotPoints = [[NSMutableArray alloc]init];
    for(int i = 0; i < (game.xDimension * game.yDimension); i++)
    {
        CGPoint point = [[game.dotArr objectAtIndex:i]dotLocation];
        [dotPoints addObject:[NSValue valueWithCGPoint:point]];
    }
    if(flag == 0)
    {
        [contentView redrawLines:game.xLineArr yLineArray:game.yLineArr AndDots:dotPoints xDim:game.xDimension yDim:game.yDimension];  
        [contentView redrawInitials:game.arrOfLabels helper:game.arrHelper xDim:game.xDimension yDim:game.yDimension p1Name:game.p1Initials p2Name:game.p2Initials];
    }
    else 
    {
        [contentView redrawLines:game.xLineArr yLineArray:game.yLineArr AndDots:dotPoints xDim:game.xDimension yDim:game.yDimension];  
        
    }
    
}

/**************************************************************************************
 * Purpose: Set up dots by calling into gameLogic's setupDot function and then interating
 * through those dots assiging them each a CGPoint on the customUIView. As well as setting
 * up button functionality in the customUINavigation toolbar.
 *
 * Reference: n/a
 **************************************************************************************/

- (void)viewWillAppear:(BOOL)animated
{
    int x, y, i, j, count;
    count = 0;
    CGFloat xtemp, ytemp;
    // Set up grid on UIView
    
    x = [gameBoard xDimension];
    y = [gameBoard yDimension];
    
    xtemp = (contentView.frame.size.width)/x;
    ytemp = (contentView.frame.size.height)/y;
    if(OGBVCPRINT)
        NSLog(@"xtemp: %f  ytemp: %f \n", xtemp, ytemp );
    
    gameBoard.dotArr = [gameBoard setUpDots:gameBoard];
    
    for(j = 0; j < y; j++)
    {
        for(i = 0; i < x; i++)
        {
            Dot *d = [gameBoard.dotArr objectAtIndex:count];
            count++;
            if(OGBVCPRINT)
                NSLog(@"Dot ID: %d -- %d  %d  %d  %d \n", d.dotID, d.up, d.down, d.left, d.right);
            
            // dotlocation was (xtemp*i)+ 25 and (ytemp*j_ + 20. Adjusted each by 10
            // so when drawing the line originates from middle of the dot object.
            
            d.dotLocation = CGPointMake((xtemp * i) + 40, (ytemp * j) + 40);
            d.dotButton.userInteractionEnabled = TRUE;
            d.dotButton.frame = CGRectMake((xtemp *i) + 30, (ytemp * j) + 30, 20, 20 );
            // IF larger hit area for UIbutton is needed use setImageInsets
            [d.dotButton addTarget:self action:@selector(touched:) forControlEvents:UIControlEventTouchDown];
            [contentView addSubview:d.dotButton];
        }
        
    }
    
    if(gameBoard.xDimension == 5)
        scrollView.zoomScale = .76;
    else if(gameBoard.xDimension == 7)
        scrollView.zoomScale = .62;
    
    [self.navigationController.toolbar clearsContextBeforeDrawing];
    CGRect frameSubmitButton = CGRectMake(30, 0, 260, 44);
    [self.navigationController.toolbar setBackgroundImage:[UIImage imageNamed:@"footer.png"] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
    
    
    [submitScore clearsContextBeforeDrawing];
    [submitScore.titleLabel clearsContextBeforeDrawing];
    submitScore.frame = frameSubmitButton;
    [submitScore addTarget:self action:@selector(verify:) forControlEvents:UIControlEventTouchDown];
    [self.navigationController.toolbar addSubview:submitScore];
    
    CGRect frameHomeLeft = CGRectMake(-2, 0, 54, 46);
    leftHome.frame = frameHomeLeft;
    [leftHome addTarget:self action:@selector(home:) forControlEvents:UIControlEventTouchDown];
    [self.navigationController.toolbar addSubview:leftHome];
    [leftHome clearsContextBeforeDrawing];
    
    CGRect frameHomeRight = CGRectMake(268 , 0, 54, 46);
    
    rightHome.frame = frameHomeRight;
    [rightHome addTarget:self action:@selector(home:) forControlEvents:UIControlEventTouchDown];
    [self.navigationController.toolbar addSubview:rightHome];
    [rightHome clearsContextBeforeDrawing];
    
    [self initializeLines: gameBoard andFlag:1];
    if(gameBoard.reload == 1)
    {
        [self initializeLines: gameBoard andFlag:0];
        if(gameBoard.playerTurn != 1)
        {
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"changeTurn"
             object:gameBoard];
        }
        gameBoard.reload = 0;
    }
    if(gameBoard.refreshBoard == 1)
        [contentView.self setNeedsDisplay];
    
    UINavigationBar *navBar = self.navigationController.navigationBar;
    [navBar clearsContextBeforeDrawing];
    if(gameBoard.playerTurn == 1)
        [navBar setBackgroundImage:[UIImage imageNamed:@"leftHeader.png"] forBarMetrics:UIBarMetricsDefault];
    else 
    {
        [navBar setBackgroundImage:[UIImage imageNamed:@"rightHeader.png"] forBarMetrics:UIBarMetricsDefault];
    }
    
    
    if(gameBoard.playAgain == 1)
    {
        [self.navigationController.toolbar clearsContextBeforeDrawing];
        [submitScore clearsContextBeforeDrawing];
        submitScore.backgroundColor = nil;
        submitScore.backgroundColor = [UIColor clearColor];
        gameBoard.playAgain = 0;
    }
    submitScore.backgroundColor = [UIColor clearColor];
    animation = [CATransition animation];
    
    
}

-(void)changeToolBarImage:(NSNotification *)notification
{
    [submitScore clearsContextBeforeDrawing];
    [submitScore.titleLabel clearsContextBeforeDrawing];
    submitScore.backgroundColor = nil;
    [self.navigationController.toolbar setBackgroundImage:[UIImage imageNamed:@"footerSelected.png"] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
    
}
/**************************************************************************************
 * Purpose: Set up scroll view functionality
 *
 * Reference: n/a
 **************************************************************************************/

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView1 {
    
    scrollView1.contentMode = (UIViewContentModeScaleAspectFit);
    return contentView;
}

/**************************************************************************************
 * Purpose: Called when application resumes after being inactive, solves bug which 
 * reloaded board but did not reload lines, called by application delegate to reload
 * dots as well ass all lines.
 *
 * Reference: Boxes_iOSApplicationAppDelegate.m
 **************************************************************************************/

-(void)coldBoot:(NSNotification *)notification
{
    if(gameBoard.reload == 1)
        [self initializeLines: gameBoard andFlag:0];
    else 
        [self initializeLines: gameBoard andFlag:1];
    
}

/**************************************************************************************
 * Purpose: Calls touched function in Dot.m which changes the image when selected and
 * also calls into gameLogic and sets that dot as the first touched.
 *
 * Reference: Dot.m
 **************************************************************************************/

-(void)touched:(id)arg
{
    //[(Dot *)arg touched:arg withGame:gameBoard];
}

/**************************************************************************************
 * Purpose: Function to handle the user tapping the finalize/set move button to finalize 
 * the line that they have touched. This function calls into the customUIView and sets
 * the first and second CGPoint accordingly. Then calls into gameLogic to handle the 
 * new move, and then calls back to the customUIView to permanently add that line to 
 * the NSMutable array of permanent moves/lines.
 *
 * Reference: customUIView.m, gameLogic.m
 **************************************************************************************/

-(void)verify:(id)arg
{
    submitScore.backgroundColor = [UIColor clearColor];
    [self.navigationController.toolbar setBackgroundImage:[UIImage imageNamed:@"footer.png"] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
    // NSLog(@"player turn = %d\n", gameBoard.playerTurn);
    // NSLog(@"updateLineArray = %d\n", gameBoard.updateLineArray);
    if(gameBoard.initialMove) //set to 1 once custom view is touched solving bug where it would change player turn
    { 
        if(OGBVCPRINT)
            NSLog(@"HERE update line array %d", gameBoard.updateLineArray);
        NSNumber *replaced = [[NSNumber alloc]initWithInt:1];
        if(gameBoard.updateLineArray == 0)//xLineArray
        {
            
            NSNumber *temp = [gameBoard.xLineArr objectAtIndex:gameBoard.indexToUpdateWith];
            if(OGBVCPRINT)
                NSLog(@"temp = %d", [temp intValue]);
            if(![temp intValue])
            {
                if(OGBVCPRINT)
                    NSLog(@"HERE for x");
                
                [gameBoard.xLineArr replaceObjectAtIndex:gameBoard.indexToUpdateWith withObject:replaced];
                
                [(customUIView *)gameBoard.dot.dotButton.superview setFirst:gameBoard.passedDotLocation];
                [(customUIView *)gameBoard.dot.dotButton.superview setSecond:gameBoard.lastTouch];
                [(customUIView *)gameBoard.dot.dotButton.superview setUserApproved:1];
                NSMutableArray *k = [gameBoard lineLogic:gameBoard];
                
                [contentView finalizedByUser];
                if(k != nil)
                    [self gameEnded:k];
            }
            
        }
        else if(gameBoard.updateLineArray == 1)//yLineArray
        {
            NSNumber *tempa = [gameBoard.yLineArr objectAtIndex:gameBoard.indexToUpdateWith];
            if(OGBVCPRINT)
                NSLog(@"HERE for y  == %d", [tempa intValue]);
            if(![tempa intValue])
            {
                if(OGBVCPRINT)
                    NSLog(@"HERE for y");
                
                [gameBoard.yLineArr replaceObjectAtIndex:gameBoard.indexToUpdateWith withObject:replaced];
                
                [(customUIView *)gameBoard.dot.dotButton.superview setFirst:gameBoard.passedDotLocation];
                [(customUIView *)gameBoard.dot.dotButton.superview setSecond:gameBoard.lastTouch];
                [(customUIView *)gameBoard.dot.dotButton.superview setUserApproved:1];
                NSMutableArray *k = [gameBoard lineLogic:gameBoard];
                
                [contentView finalizedByUser];
                if(k != nil)
                    [self gameEnded:k];
            }
            
        }
    }
    
}


/**************************************************************************************
 * Purpose: Notification function that is called from gameLogic when a player has won
 * lost or tied a game. Prompts the user with a dialoge to either play again or to 
 * go back to the main menu. 
 *
 * Reference: n/a
 **************************************************************************************/

-(void)gameEnded:(NSMutableArray *)temp
{
    gameBoard.reload = -10;
    notificationID = 0;
    NSString *winner = [temp objectAtIndex:0];
    NSString *passedmsg = [temp objectAtIndex:1];
    NSString *message;
    
    if([winner isEqualToString:gameBoard.player1])
        message = passedmsg;
    else if([winner isEqualToString:gameBoard.player2])
        message = passedmsg;
    else {
        message = @"\nLooks like it was a tie game. Time for a rematch.";
    }
    alert = [BlockAlertView alertWithTitle:@"Game Over" message:message];
    __weak onlineGameBoardViewController *weakSelf = self;
    __weak gameLogic *weakboard = gameBoard;
    __weak CATransition *tempAnimation = animation;
    
    [alert setCancelButtonWithTitle:@"Return Home" block:^{
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"removeNavBarInfo" object:nil];
        [weakSelf saveData:weakboard];
        
        tempAnimation.subtype = kCATransitionFromBottom;
        tempAnimation.fillMode = kCAFillModeBackwards;
        tempAnimation.startProgress = 0;
        [tempAnimation setRemovedOnCompletion:YES];
        [tempAnimation setDelegate:weakSelf];
        [tempAnimation setDuration:0.5];
        [tempAnimation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
        tempAnimation.type = @"pageUnCurl";
        
        [weakSelf.navigationController.view.layer addAnimation:tempAnimation forKey:@"pageCurlAnimation"];
        [weakboard resetGame:weakboard option:0];
        weakSelf.navigationController.toolbarHidden = TRUE;
        [weakSelf.navigationController popToRootViewControllerAnimated:NO];
        
    }];
    
    [alert show];
    
    gameOver = 1;
}

/**************************************************************************************
 * Purpose: Home button functionality, pose the user with a UIAlertView to choose if 
 * they want to continue playing thier game or quit to the home screen.
 *
 * Reference: n/a
 **************************************************************************************/

-(void)home:(id)arg
{
    notificationID = 1;
    alert2 = [BlockAlertView alertWithTitle:@"Leave Game" message:@"\nDo you want to leave? Data will be saved."];
    
    __weak onlineGameBoardViewController *weakSelf = self;
    __weak gameLogic *weakboard = gameBoard;
    __weak UIButton *weakLeft = leftHome;
    __weak UIButton *weakRight = rightHome;
    [alert2 setCancelButtonWithTitle:@"Yes" block:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"removeNavBarInfo"
                                                            object:nil];
        [weakSelf saveData:weakboard];
        [weakboard resetGame:weakboard option:0];
        [weakLeft setBackgroundColor:[UIColor clearColor]];
        [weakRight setBackgroundColor:[UIColor clearColor]];
        [weakSelf transitions];
        
    }];
    [alert2 setDestructiveButtonWithTitle:@"No" block:nil];
    [alert2 show];
    
}


-(void)transitions
{
    [leftHome clearsContextBeforeDrawing];
    [rightHome clearsContextBeforeDrawing];
    self.navigationController.toolbarHidden = TRUE;
    [animation setDelegate:self];
    [animation setDuration:0.5];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
    animation.type = @"pageUnCurl";
    
    animation.subtype = kCATransitionFromBottom;
    animation.fillMode = kCAFillModeBackwards;
    animation.startProgress = 0;
    [animation setRemovedOnCompletion:YES];
    
    [self.navigationController.view.layer addAnimation:animation forKey:@"pageCurlAnimation"];
    [self.navigationController popToRootViewControllerAnimated:NO];
    
}

-(void)saveData:(gameLogic *)gBoard
{
    if([gBoard.player1 length] > 0 && [gBoard.player2 length] > 0 && gBoard.p1Score != 1000 && gBoard.xLineArr != nil)
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
        docs = [paths  objectAtIndex:0];
        fm = [[NSFileManager alloc]init];
        NSError *err;
        NSString *myFolder;
        [fm createDirectoryAtPath:docs withIntermediateDirectories:NO attributes:nil error:&err];
        
        if(gBoard.onlineGame)
            myFolder = [docs stringByAppendingFormat:@"/12Dot/Online"];
        else if(!gBoard.onlineGame)
            myFolder = [docs stringByAppendingFormat:@"/12Dot/Local"];
        
        BOOL help = [fm createDirectoryAtPath:myFolder withIntermediateDirectories:NO attributes:nil error:&err];
        if(OGBVCPRINT)
        NSLog(@"HELP = %d error = %@\n", help, [err localizedDescription]);
        
        BOOL exists = [fm fileExistsAtPath:myFolder];
        if(OGBVCPRINT)
        NSLog(@"Data exists at %@: %d", myFolder, [[NSNumber numberWithBool:exists]intValue]);
        
        if(!exists)
        {
            err = nil;
            [fm createDirectoryAtPath:myFolder withIntermediateDirectories:NO attributes:nil error:&err];
        }
        
        data.xLineArray        = [NSMutableArray arrayWithArray:gBoard.xLineArr];
        data.yLineArray        = [NSMutableArray arrayWithArray:gBoard.yLineArr];
        data.initialsCGRect    = [NSMutableArray arrayWithArray:gBoard.arrOfLabels];
        data.initialsPlayer    = [NSMutableArray arrayWithArray:gBoard.arrHelper];
        data.p1                = gBoard.player1;
        data.p1score           = [NSNumber numberWithInt:gBoard.p1Score];
        data.p1initials        = gBoard.p1Initials;
        data.p2                = gBoard.player2;
        data.p2score           = [NSNumber numberWithInt:gBoard.p2Score];
        data.p2initials        = gBoard.p2Initials;
        data.turn              = [NSNumber numberWithInt:gBoard.playerTurn];
        data.xDim              = [NSNumber numberWithInt:gBoard.xDimension];
        data.yDim              = [NSNumber numberWithInt:gBoard.yDimension];
        data.gameDataID        = [NSNumber numberWithInt:gBoard.gameDataID];
        NSData *moidata = [NSKeyedArchiver archivedDataWithRootObject:data];
        
        if(gBoard.gameDataID == 1)
            moifile = [myFolder stringByAppendingPathComponent:@"gameData1"];
        else if(gBoard.gameDataID == 2)
            moifile = [myFolder stringByAppendingPathComponent:@"gameData2"];
        else if(gBoard.gameDataID == 3)
            moifile = [myFolder stringByAppendingPathComponent:@"gameData3"];
        else if(gBoard.gameDataID == 4)
            moifile = [myFolder stringByAppendingPathComponent:@"gameData4"];
        else if(gBoard.gameDataID == 5)
            moifile = [myFolder stringByAppendingPathComponent:@"gameData5"];
        else if(gBoard.gameDataID == 6)
            moifile = [myFolder stringByAppendingPathComponent:@"gameData6"];
        else if(gBoard.gameDataID == 7)
            moifile = [myFolder stringByAppendingPathComponent:@"gameData7"];
        else if(gBoard.gameDataID == 8)
            moifile = [myFolder stringByAppendingPathComponent:@"gameData8"];
        else if(gBoard.gameDataID == 9)
            moifile = [myFolder stringByAppendingPathComponent:@"gameData9"];
        else if(gBoard.gameDataID == 10)
            moifile = [myFolder stringByAppendingPathComponent:@"gameData10"];
        
        exists = [fm fileExistsAtPath:moifile];
        if(exists)
        {
            [fm removeItemAtPath:moifile error:&err];
        }
        if(OGBVCPRINT)
        NSLog(@"moidata = %@ gboard.id = %d, exists = %d myfolder = %@\n", moifile, gBoard.gameDataID, exists, myFolder);
        if(!gameOver)
            [moidata writeToFile:moifile atomically:YES];
        if(OGBVCPRINT)
        NSLog(@"moidata = %@ gboard.id = %d, exists = %d myfolder = %@\n", moifile, gBoard.gameDataID, exists, myFolder);
    }
    
}

/**************************************************************************************
 * Purpose: Wrapper function that takes the mutable array passed by the notification
 * that triggers drawInitials and then forwards that information to the customUIView
 * to draw to the gameboard.
 *
 * Reference: customUIView.m
 **************************************************************************************/

-(void)drawInitials:(NSNotification *)notification
{
    NSMutableArray *arr = [notification object];
    [contentView drawInitials:arr];
} 


/**************************************************************************************
 * Purpose: Currently set up as an instant win button for testing purposes, will eventually
 * be a settings button or a chat button for online play hopefully.
 *
 * Reference: n/a
 **************************************************************************************/


- (void)didReceiveMemoryWarning
{
    if(OGBVCPRINT)
    NSLog(@"did recieve mem warning\n");
    // Releases the view if it doesn't have a superview.
    //gameBoard.refreshBoard = -10;
    gameBoard.reload = 1;
    
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [self initializeLines: gameBoard andFlag:1];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setToolbarHidden:YES];
    [submitScore removeFromSuperview];
    
}

- (void)viewDidUnload
{
    if(OGBVCPRINT)
    NSLog(@"view did unload\n");
    //gameBoard.refreshBoard = -10;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
