//
//  gameData.m
//  Boxes_iOSApplication
//
//  Created by NICK ALTO on 2/25/12.
//  Copyright (c) 2012 Oregon State University. All rights reserved.
//

#import "gameData.h"
BOOL GAMEDATAPRINT = FALSE;

@implementation gameData
@synthesize xLineArray          = _xLineArray;
@synthesize yLineArray          = _yLineArray;
@synthesize initialsCGRect      = _initialsCGRect;
@synthesize initialsPlayer      = _initialsPlayer;
@synthesize p1                  = _p1;
@synthesize p1score             = _p1score;
@synthesize p1initials          = _p1initials;
@synthesize p2                  = _p2;
@synthesize p2score             = _p2score;
@synthesize p2initials          = _p2initials;
@synthesize turn                = _turn;
@synthesize xDim                = _xDim;
@synthesize yDim                = _yDim;
@synthesize gameDataID          = _gameDataID;
@synthesize boxbotflag          = _boxbotflag;

/**************************************************************************************
 * Purpose: Simple Init method that initializes the NSMutable Array's when an instance
 * of gameData is created in Boxes App Delegate
 *
 * Reference: Called only from the Boxes Delegate when the game is started and when it 
 * gets terminated. 
 **************************************************************************************/

-(id)init
{
    self = [super init];
    if (self) {
        self.xLineArray     = [[NSMutableArray alloc]init];
        self.yLineArray     = [[NSMutableArray alloc]init];
        self.initialsCGRect = [[NSMutableArray alloc]init];
        self.initialsPlayer = [[NSMutableArray alloc]init];

    }
    return self;
}

/**************************************************************************************
 * Purpose: InitWithCoder method called when starting a new game, first check to see 
 * if there is any data saved at a specific location. If so restore and initialize with
 * this funciton.
 *
 * Reference: Called only from the Boxes Delegate when the game is started
 **************************************************************************************/

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        self.xLineArray     = [aDecoder decodeObjectForKey:@"xLineArray"];
        self.yLineArray     = [aDecoder decodeObjectForKey:@"yLineArray"];
        self.initialsCGRect = [aDecoder decodeObjectForKey:@"initialsCGRect"];
        self.initialsPlayer = [aDecoder decodeObjectForKey:@"initialsPlayer"];
        self.p1             = [aDecoder decodeObjectForKey:@"p1"];
        self.p1score        = [aDecoder decodeObjectForKey:@"p1score"];
        self.p1initials     = [aDecoder decodeObjectForKey:@"p1initials"];
        self.p2             = [aDecoder decodeObjectForKey:@"p2"];
        self.p2score        = [aDecoder decodeObjectForKey:@"p2score"];
        self.p2initials     = [aDecoder decodeObjectForKey:@"p2initials"];
        self.turn           = [aDecoder decodeObjectForKey:@"turn"];
        self.xDim           = [aDecoder decodeObjectForKey:@"xDim"];
        self.yDim           = [aDecoder decodeObjectForKey:@"yDim"];
        self.gameDataID     = [aDecoder decodeObjectForKey:@"gameDataID"];
        self.boxbotflag     = [aDecoder decodeObjectForKey:@"boxbotflag"];

    }
    return self;
}

/**************************************************************************************
 * Purpose: Encodes data right after assigning all the data from gameLogic to the 
 * gameData instance in the Boxes App Delegate, saving it to file, see saveData method
 * in the Application Delegate. 
 *
 * Reference: Called only from the Boxes Delegate when the game is sent to the background
 **************************************************************************************/

-(void)encodeWithCoder:(NSCoder *)aCoder
{
    if(GAMEDATAPRINT)
        NSLog(@"encoding p1 = %@ x = %d y = %d\n\n", self.p1, [self.xDim intValue], [self.yDim intValue]);
    
    [aCoder encodeObject:self.xLineArray forKey:@"xLineArray"];
    [aCoder encodeObject:self.yLineArray forKey:@"yLineArray"];
    [aCoder encodeObject:self.initialsCGRect forKey:@"initialsCGRect"];
    [aCoder encodeObject:self.initialsPlayer forKey:@"initialsPlayer"];
    [aCoder encodeObject:self.p1 forKey:@"p1"];
    [aCoder encodeObject:self.p1score forKey:@"p1score"];
    [aCoder encodeObject:self.p1initials forKey:@"p1initials"];
    [aCoder encodeObject:self.p2 forKey:@"p2"];
    [aCoder encodeObject:self.p2score forKey:@"p2score"];
    [aCoder encodeObject:self.p2initials forKey:@"p2initials"];
    [aCoder encodeObject:self.turn forKey:@"turn"];
    [aCoder encodeObject:self.xDim forKey:@"xDim"];
    [aCoder encodeObject:self.yDim forKey:@"yDim"];
    [aCoder encodeObject:self.gameDataID forKey:@"gameDataID"];
    [aCoder encodeObject:self.boxbotflag forKey:@"boxbotflag"];
}

@end
