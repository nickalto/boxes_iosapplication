//
//  customUIView.m
//  Boxes_iOSApplication
//
//  Created by NICK ALTO on 2/1/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "customUIView.h"

BOOL CUSTOMUIVIEWPRINT = FALSE;

//statement - gameLogic - dot - customNavigation - all viewControllers

@implementation customUIView
@synthesize first                           = _first;
@synthesize second                          = _second;
@synthesize saveContextAndCommitDrawing     = _saveContextAndCommitDrawing;
@synthesize draw                            = _draw;
@synthesize userApproved                    = _userApproved;
@synthesize locArr                          = _locArr;
@synthesize numberFloats                    = _numberFloats;


/**************************************************************************************
 * Purpose: Initializes customUIView as well as variables and registers for required
 * notifications. 
 *
 * Reference: Called by gameBoardViewController, the only viewController that initializes
 * an instance of the customUIView. 
 **************************************************************************************/

- (id)initWithFrame:(CGRect)frame and:(id)g 
{
    self = [super initWithFrame:frame];
    if (self) {
        
        saveContextAndCommitDrawing = false;
        draw                        = false;
        numberFloats                = 0;
        locArr = [[NSMutableArray alloc]init];
        //self.clearsContextBeforeDrawing = NO;
        
        //notification from Dot.m
        [[NSNotificationCenter defaultCenter]
         addObserver:self
         selector:@selector(touched:)
         name:@"touched"
         object:nil];
        
        //notification from gameLogic - reset
        [[NSNotificationCenter defaultCenter]
         addObserver:self
         selector:@selector(removeNotifications:)
         name:@"removeNotifications"
         object:nil];
        
        //notification from gameLogic - closestDot
        [[NSNotificationCenter defaultCenter]
         addObserver:self
         selector:@selector(updateLine:)
         name:@"calculatedDots"
         object:nil];
        
        //notification from gameLogic - reset
        [[NSNotificationCenter defaultCenter]
         addObserver:self
         selector:@selector(erase:)
         name:@"eraseData"
         object:nil];
    }

    return self;
}

/**************************************************************************************
 * Purpose: When passed in a NSMutable array through notification it sets the first 
 * and second dots as given by CGRect's and sets the draw flag to draw the given line 
 * but not store it, the move has not been finalized yet. 
 *
 * Reference: Called by gameLogic 
 **************************************************************************************/

-(void)updateLine:(NSNotification *)notification
{
    NSMutableArray *arr = [notification object];
    first = [[arr objectAtIndex:0]CGPointValue];
    second = [[arr objectAtIndex:1]CGPointValue];
    if(CUSTOMUIVIEWPRINT)
        NSLog(@"update line first = %f, %f   second = %f, %f", first.x, first.y, second.x, second.y);
     draw = true;
    saveContextAndCommitDrawing = false;
    [self setNeedsDisplay];
}

/**************************************************************************************
 * Purpose: Called by gameBoardViewController when the user clicks on the navigation
 * bar to finalize move, which adds it to the NSMutable array locArr as a permanent
 * move. 
 *
 * Reference: Called by gameBoardViewController
 **************************************************************************************/

-(void)finalizedByUser
{
    if(CUSTOMUIVIEWPRINT)
        NSLog(@"\n\nfinalizedByUser: recieved!!!\n\n");
    draw = true;
    userApproved = true;
    saveContextAndCommitDrawing = true;
    [self setNeedsDisplay];
}
-(void)touched:(NSNotification *)notification
{
    if(CUSTOMUIVIEWPRINT)
        NSLog(@"Touched\n");
     draw = true;
    [self setNeedsDisplay];
}

/**************************************************************************************
 * Purpose: Called by gameBoardViewController when a gameboard/data is reloaded following
 * a restore from memory (killing app and restoring after saved to disk), or restoring
 * game during online gameplay, after each move. Iterate through passed in xLineArray
 * find each line that whose value is 1, calculate dots and redraw the line by adding
 * those two points to the locArr, which stores all lines previous recorded. Do this for 
 * xLineArr, and yLineArr and then redraw initials for boxes that have been filled in.
 *
 * Reference: Called by gameBoardViewController
 **************************************************************************************/

-(void)redrawLines:(NSMutableArray *)xLineArr yLineArray:(NSMutableArray *)yLineArr AndDots:(NSMutableArray *)dotArray xDim:(int)xDim yDim:(int)yDim
{
    if(CUSTOMUIVIEWPRINT)
        NSLog(@"redrawLines xDim = %d,  yDim = %d\n", xDim, yDim);
    int xLineVar = (xDim -1) * yDim;
    int yLineVar = (yDim -1) * xDim;
    for(int i = 0; i < xLineVar; i++)
    {
        if(CUSTOMUIVIEWPRINT)
            NSLog(@"iterating through xLineArr i = %d, intValue = %d\n", i, [[xLineArr objectAtIndex:i]intValue]);
        if([[xLineArr objectAtIndex:i]intValue])
        {
            int row = i/(xDim -1);
            int dotLeftLocation = i + row;
            int dotRightLocation = i + row + 1;
            CGPoint dotLeft = [[dotArray objectAtIndex:dotLeftLocation]CGPointValue]; 
            CGPoint dotRight = [[dotArray objectAtIndex:dotRightLocation]CGPointValue];
            NSNumber *temp = [[NSNumber alloc]initWithFloat:dotLeft.x];
            NSNumber *temp2 = [[NSNumber alloc]initWithFloat:dotLeft.y];
            NSNumber *temp3 = [[NSNumber alloc]initWithFloat:dotRight.x];
            NSNumber *temp4 = [[NSNumber alloc]initWithFloat:dotRight.y];
            [locArr addObject:temp];
            [locArr addObject:temp2];
            [locArr addObject:temp3];
            [locArr addObject:temp4];
            numberFloats++;
        }
    }
    
    for(int i = 0; i < yLineVar; i++)
    {
        if([[yLineArr objectAtIndex:i]intValue])
        {
            int dotUpLocation = i;
            int dotBelowLocation = i + xDim;
            CGPoint dotUp = [[dotArray objectAtIndex:dotUpLocation]CGPointValue];
            CGPoint dotDown = [[dotArray objectAtIndex:dotBelowLocation]CGPointValue];
            NSNumber *temp = [[NSNumber alloc]initWithFloat:dotUp.x];
            NSNumber *temp2 = [[NSNumber alloc]initWithFloat:dotUp.y];
            NSNumber *temp3 = [[NSNumber alloc]initWithFloat:dotDown.x];
            NSNumber *temp4 = [[NSNumber alloc]initWithFloat:dotDown.y];
            [locArr addObject:temp];
            [locArr addObject:temp2];
            [locArr addObject:temp3];
            [locArr addObject:temp4];
            numberFloats++;
        }
    }
    draw = true;
    userApproved = true;
    saveContextAndCommitDrawing = true;
    [self setNeedsDisplay];
    
}

/**************************************************************************************
 * Purpose: Called by gameBoardViewController when a gameboard/data is reloaded following
 * a restore from memory (killing app and restoring after saved to disk), or restoring
 * game during online gameplay, after each move. Redraws each tile/box that a player has
 * won and draws the player's initial in the box.
 *
 * Reference: Called by gameBoardViewController
 **************************************************************************************/

- (void)redrawInitials:(NSMutableArray *)initialsArr helper:(NSMutableArray *)helperArr xDim:(int)xDim yDim:(int)yDim p1Name:(NSString *)p1 p2Name:(NSString*)p2
{
    NSMutableArray *passArr = [[NSMutableArray alloc]init];
    int size = (xDim -1) * (yDim -1);
    for(int i = 0; i < size; i++)
    {
        NSNumber *temp = [helperArr objectAtIndex:i];
        if(CUSTOMUIVIEWPRINT)
            NSLog(@"i = %d p1 = %@  p2 = %@\n", [temp intValue], p1, p2);

        if([temp intValue] > 0)
        {
            CGRect tempPoint = [[initialsArr objectAtIndex:i]CGRectValue];
            NSNumber *xLoc = [[NSNumber alloc]initWithFloat:tempPoint.origin.x];
            NSNumber *yLoc = [[NSNumber alloc]initWithFloat:tempPoint.origin.y];
            NSNumber *width = [[NSNumber alloc]initWithFloat:tempPoint.size.width];
            NSNumber *height = [[NSNumber alloc]initWithFloat:tempPoint.size.height];
            [passArr addObject:xLoc];
            [passArr addObject:yLoc];
            [passArr addObject:width];
            [passArr addObject:height];
            int value = [temp intValue];
            NSString *player;
            
                if(value == 2)
                    player = [[NSString alloc]initWithFormat:@"%@", p2];
                else 
                    player = [[NSString alloc]initWithFormat:@"%@", p1];
            if(CUSTOMUIVIEWPRINT)
                NSLog(@"player = %@\n", player);

            [passArr addObject:player];
            [self drawInitials:passArr];
            [passArr removeAllObjects];

        }
    }
}

-(void)removeNotifications:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"eraseData" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"calculatedDots" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"touched" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"removeNotifications" object:nil];

}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    saveContextAndCommitDrawing = true;
    draw = true;
    [self setNeedsDisplay];
    [super touchesEnded:touches withEvent:event];
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    draw = true;
    [self setNeedsDisplay];
}

-(void)erase:(NSNotification *)notification
{
    locArr = nil;
    numberFloats = 0;
    draw = false;
    userApproved = false;
    saveContextAndCommitDrawing = false;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch* touch = [touches anyObject];
    CGPoint startPoint = [touch locationInView:self];
    NSValue* val = [NSValue valueWithCGPoint:startPoint];
    [[NSNotificationCenter defaultCenter]
        postNotificationName:@"touchesBeganCanvas"
        object:val];

    draw = true;
    [self setNeedsDisplay];
    [super touchesBegan:touches withEvent:event];
}

-(void)drawInitials:(NSMutableArray *)arr
{
    CGPoint start = CGPointMake([[arr objectAtIndex:0]floatValue], [[arr objectAtIndex:1]floatValue]);
    int width = [[arr objectAtIndex:2]intValue];
    int height = [[arr objectAtIndex:3] intValue];
    NSString *initials = [arr objectAtIndex:4];
    //changed value to accomodate the font 
    start.y +=5;
    CGRect newRect = CGRectMake(start.x, start.y, width, height);
    UILabel *initialsLabel = [[UILabel alloc]initWithFrame:newRect];
    initialsLabel.textColor = [UIColor blackColor];
    initialsLabel.backgroundColor = [UIColor clearColor];
    initialsLabel.text = initials;
    initialsLabel.textAlignment = UITextAlignmentCenter;
    UIFont *sFont = [UIFont fontWithName:@"FFFTusj-Bold" size:30];
    [initialsLabel setFont:sFont];
    [self addSubview:initialsLabel];
    
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.

- (void)drawRect:(CGRect)rect
{
    if(CUSTOMUIVIEWPRINT)
        NSLog(@"draw = %d  userapproved = %d  saveContext = %d\n", draw, userApproved, saveContextAndCommitDrawing);
    if(draw && userApproved && saveContextAndCommitDrawing)
    {
        if(CUSTOMUIVIEWPRINT)
            NSLog(@"In Draw rect\n");
            NSNumber *temp = [[NSNumber alloc]initWithFloat:first.x];
            NSNumber *temp2 = [[NSNumber alloc]initWithFloat:first.y];
            NSNumber *temp3 = [[NSNumber alloc]initWithFloat:second.x];
            NSNumber *temp4 = [[NSNumber alloc]initWithFloat:second.y];
            [locArr addObject:temp];
            [locArr addObject:temp2];
            [locArr addObject:temp3];
            [locArr addObject:temp4];
            numberFloats++;
    }
    
    BOOL doubledraw = (draw && userApproved && saveContextAndCommitDrawing);
    if(draw)
    {
        if(CUSTOMUIVIEWPRINT)
            NSLog(@"first level\n");
         CGContextRef previousCanvas = UIGraphicsGetCurrentContext();

        if(numberFloats)
        {
            if(CUSTOMUIVIEWPRINT)
                NSLog(@"drawing locArray numberFloats = %d  locArray count = %d\n", numberFloats, [self.locArr count] );
            int i, j;
            for(j = 0; j < numberFloats; j++)
            {
                i = j * 4;
                CGContextSetRGBStrokeColor(previousCanvas, .2588, .2588, .2588, .7);
                CGContextSetLineWidth(previousCanvas, 1.5);

            CGContextMoveToPoint(previousCanvas, [[locArr objectAtIndex:i]floatValue], [[locArr objectAtIndex:i+1]floatValue]);
            CGContextAddLineToPoint(previousCanvas, [[locArr objectAtIndex:i+2]floatValue], [[locArr objectAtIndex:i+3]floatValue]);
                CGContextStrokePath(previousCanvas);
            }
        }
        if(!doubledraw)
        {
            CGContextSetRGBStrokeColor(previousCanvas, .7843, .2588, .2588, .9);
            CGContextMoveToPoint(previousCanvas, first.x, first.y);
            CGContextAddLineToPoint(previousCanvas, second.x, second.y);
            CGContextSetLineWidth(previousCanvas, 1.5);

            CGContextStrokePath(previousCanvas);
        }

    }
    if(CUSTOMUIVIEWPRINT)
        NSLog(@"value of draw = %d value of final = %d and saveContextAndCommitDrawing = %d\n", draw, userApproved, saveContextAndCommitDrawing);
     draw = false;
     userApproved = false;
     saveContextAndCommitDrawing = false;

        [super drawRect:rect];
}


@end
