//
//  firstViewController.m
//  Boxes_iOSApplication
//
//  Created by NICK ALTO on 1/30/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "firstViewController.h"
#import "BlockAlertView.h"
#import "InfoViewController.h"
#import "onlineGameBoardViewController.h"

@interface firstViewController()
{
    CATransition *animation;
    BlockAlertView *alert;
}
@end

BOOL FIRSTVCPRINT = FALSE;
@implementation firstViewController
@synthesize gameBoard           = _gameBoard;
@synthesize gData               = _gData;


/**************************************************************************************
 * Purpose: Initalizes firstViewController and sets the gameLogic gameBoard instance,
 * as well as sets up the background color
 *
 * Reference: Called by Boxes Application Delegate
 **************************************************************************************/

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andBoard:(gameLogic *)game andData:(gameData *)data
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        gameBoard               = game;
        gData                   = data;
    }
    return self;
}

/**************************************************************************************
 * Purpose: If the user has already started a game and has terminated the app this will
 * give the user a dialoge box to continue playing where he/she left off or to start a
 * brand new game.
 *
 * Reference: n/a
 **************************************************************************************/

- (void)viewWillAppear:(BOOL)animated
{

    if(IS_IPHONE5)
        [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"i5.png"]]];
    else
        [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"plainBackground.jpg"]]];

        UINavigationBar *navBar = self.navigationController.navigationBar;
    [navBar clearsContextBeforeDrawing];
    
    UIFont *sFont = [UIFont fontWithName:@"FFFTusj-Bold" size:24];

    onlineGameButton.titleLabel.textColor = [UIColor whiteColor];
    [onlineGameButton.titleLabel setFont:sFont];
    [onlineGameButton setTitle:@" Online Game " forState:UIControlStateNormal];
    [onlineGameButton setTitleColor:[UIColor colorWithRed:.2509 green:.2157 blue:.1843 alpha:1] forState:UIControlStateNormal];
    [onlineGameButton setBackgroundImage:[UIImage imageNamed:@"FVbutton2.png"] forState:UIControlStateNormal];
    [onlineGameButton setBackgroundImage:[UIImage imageNamed:@"FVbutton2Selected.png"] forState:UIControlStateHighlighted];
    [onlineGameButton setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];

    [localGameButton.titleLabel setFont:sFont];
    [localGameButton setTitle:@" Local Game " forState:UIControlStateNormal];
    [localGameButton setTitleColor:[UIColor colorWithRed:.2509 green:.2157 blue:.1843 alpha:1] forState:UIControlStateNormal];
    [localGameButton setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
    [localGameButton setBackgroundImage:[UIImage imageNamed:@"FVbutton1.png"] forState:UIControlStateNormal];
    [localGameButton setBackgroundImage:[UIImage imageNamed:@"FVbutton1Selected.png"] forState:UIControlStateHighlighted];
    
    [instructionButton setBackgroundImage:[UIImage imageNamed:@"logo.png"] forState:UIControlStateNormal];
    [instructionButton setBackgroundImage:[UIImage imageNamed:@"logoSelected.png"] forState:UIControlStateHighlighted];

    animation = [CATransition animation];


    
    if (![@"1" isEqualToString:[[NSUserDefaults standardUserDefaults]
                                objectForKey:@"Avalue"]]) {
        
        alert = [BlockAlertView alertWithTitle:@"Welcome" message:@"\nTo view instructions tap on the BoxDot Logo in the bottom left corner. Enjoy!"];
        [alert setDestructiveButtonWithTitle:@"Dismiss" block:nil];
        [alert show];
    }
    
}


/**************************************************************************************
 * Purpose: Logic behind the dialoge box above allowing the user to continue or to start
 * a brand new game.
 *
 * Reference: called by continueGame (above)
 **************************************************************************************/


#pragma mark - View lifecycle

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

/**************************************************************************************
 * Purpose: Logic behind the beginOnline Game button on the home screen, not yet implemented
 * need to do more research on game center implementation.
 *
 * Reference: n/a
 **************************************************************************************/

- (IBAction)beginOnlineGame:(id)sender
{

    if(FIRSTVCPRINT)
        NSLog(@"BeginOnlineGame was called");

    alert = [BlockAlertView alertWithTitle:@"Coming Soon" message:@"\nGame center integration is currently being developed."];
    [alert setDestructiveButtonWithTitle:@"Dismiss" block:nil];

    [alert show];


 }


-(IBAction)information:(id)sender
{

    InfoViewController *info = [[InfoViewController alloc]initWithNibName:@"InfoViewController" bundle:nil andInt:1 andVC:self];
    [animation setDelegate:self];
    [animation setDuration:0.5];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
    animation.type = @"pageUnCurl";
    
    animation.subtype = kCATransitionFromBottom;
    animation.fillMode = kCAFillModeBackwards;
    animation.startProgress = 0;
    [animation setRemovedOnCompletion:YES];
    
    [self.navigationController.view.layer addAnimation:animation forKey:@"pageUnCurlAnimation"];
    [self.navigationController pushViewController:info animated:NO];
}

/**************************************************************************************
 * Purpose: Logic behind begin local game button on the home screen to begin a pass
 * and play local game, pushes next view onto the customNavigationController.
 *
 * Reference: n\a
 **************************************************************************************/

- (IBAction)beginLocalGame:(id)sender
{
    if(FIRSTVCPRINT)
        NSLog(@"BeginLocalGame was called");
    gameOptionViewController *secondView = [[gameOptionViewController alloc]initWithNibName:@"gameOptionViewController" bundle:nil andBoard:gameBoard andData:gData];
    gameBoard.onlineGame = 0;
    self.navigationController.view.autoresizesSubviews = FALSE;

    [animation setDelegate:self];
    [animation setDuration:0.5];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
    animation.type = @"pageCurl";
    
    animation.subtype = kCATransitionFromTop;
    animation.fillMode = kCAFillModeForwards;
    [animation setRemovedOnCompletion:YES];
    
    [self.navigationController.view.layer addAnimation:animation forKey:@"pageCurlAnimation"];
    [self.navigationController pushViewController:secondView animated:NO];


}

@end
