//
//  secondViewController.m
//  Boxes_iOSApplication
//
//  Created by NICK ALTO on 1/30/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "secondViewController.h"

BOOL SECONDVCPRINT = FALSE;

@implementation secondViewController
@synthesize gameBoard = _gameboard;
@synthesize gData     = _gData;

CATransition *animation;
BlockAlertView *alert;
CGPoint originalCenter;                     // used to scale view up and down for keyboard
CGPoint CENTER;
CGFloat release_offset = 0;
CGFloat y_center = 0;
UIColor *textColor;
UIFont *sFont;


int AIFlag = 0;

/**************************************************************************************
 * Purpose: Initialize secondViewController as well as set background image. Make sure
 * to uncomment begin game when ready to ship so user's cannot begin a game until they
 * have each entered a user name
 *
 * Reference: n/a
 **************************************************************************************/

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andBoard:(gameLogic *)game andData:(id)g
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        gData = g;
        gameBoard = game;
        textColor = [UIColor colorWithRed:.2509 green:.2157 blue:.1843 alpha:1];

        if(IS_IPHONE5)
            [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"i5.png"]]];
        else
            [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"plainBackground.jpg"]]];
        
        //set up custom UI
        sFont = [UIFont fontWithName:@"FFFTusj-Bold" size:24];


        textfield1.image = textfield2.image = [UIImage imageNamed:@"textbox.png"];
        
        [p1OptionButton setBackgroundImage:[UIImage imageNamed:@"SVOptionButton.png"] forState:UIControlStateNormal];
        [p1OptionButton setBackgroundImage:[UIImage imageNamed:@"SVOptionButtonSelected.png"] forState:UIControlStateSelected];

        [p2OptionButton setBackgroundImage:[UIImage imageNamed:@"SVOptionButton.png"] forState:UIControlStateNormal];
        [p2OptionButton setBackgroundImage:[UIImage imageNamed:@"SVOptionButtonSelected.png"] forState:UIControlStateSelected];
        
        [p1OptionButton addTarget:self action:@selector(GameOptions:) forControlEvents:UIControlEventTouchUpInside];
        [p2OptionButton addTarget:self action:@selector(GameOptions:) forControlEvents:UIControlEventTouchUpInside];
        
        [fiveXSix setBackgroundImage:[UIImage imageNamed:@"SVleftButton.png"] forState:UIControlStateNormal];
        [fiveXSix setBackgroundImage:[UIImage imageNamed:@"SVleftButtonSelected.png"] forState:UIControlStateHighlighted];
        [fiveXSix setBackgroundImage:[UIImage imageNamed:@"SVleftButtonEmpty.png"] forState:UIControlStateNormal];
        [fiveXSix.titleLabel setFont:sFont];
        
        
        [sixXSeven setBackgroundImage:[UIImage imageNamed:@"SVrightButton.png"] forState:UIControlStateNormal];
        [sixXSeven setBackgroundImage:[UIImage imageNamed:@"SVrightButtonSelected.png"] forState:UIControlStateHighlighted];
        [sixXSeven setBackgroundImage:[UIImage imageNamed:@"SVrightButtonEmpty.png"] forState:UIControlStateNormal];

        [player1Check setBackgroundImage:[UIImage imageNamed:@"SVfirstPlayerRed.png"] forState:UIControlStateNormal];
        player1Check.userInteractionEnabled = FALSE;
        
        [player2Check setBackgroundImage:[UIImage imageNamed:@"SVfirstPlayerRed.png"] forState:UIControlStateNormal];
        player2Check.userInteractionEnabled = FALSE;
        
        CGFloat x_center = self.view.center.x;
        CGFloat y_center = self.view.center.y;
        CENTER = CGPointMake(x_center, y_center);
        
        [sixXSeven.titleLabel setFont:sFont];
        [p1label setFont:sFont];
        [p2label setFont:sFont];
        [beginGame.titleLabel setFont:sFont];
        animation = [CATransition animation];
        
        [gameBoard setXDimension:5];
        [gameBoard setYDimension:6];
        
        originalCenter = CGPointMake(self.view.center.x, self.view.center.y - 20);
        
        if (!gameBoard.reload) {
            gameBoard.playerTurn = 1;
        }
        beginGame.alpha = .6;
        beginGame.enabled = FALSE;
        [beginGame setTitleColor:textColor forState:UIControlStateNormal];
        
        gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewWillAppear:(BOOL)animated
{
    [beginGame setBackgroundImage:[UIImage imageNamed:@"SVstartGameDisabled.png"] forState:UIControlStateDisabled];
    beginGame.enabled = FALSE;
    beginGame.alpha = 0.6;
    [beginGame setTitleColor:textColor forState:UIControlStateDisabled];
    [beginGame setTitleColor:textColor forState:UIControlStateNormal];
    
    p1label.textColor = p2label.textColor = textColor;
    [p1label setValue:[UIColor colorWithRed:.2509 green:.2157 blue:.1843 alpha:.4] forKeyPath:@"_placeholderLabel.textColor"];
    
    [p2label setValue:[UIColor colorWithRed:.2509 green:.2157 blue:.1843 alpha:.4] forKeyPath:@"_placeholderLabel.textColor"];

    if(gameBoard.xDimension == 5 || gameBoard.xDimension == 0)
    {
        [fiveXSix setBackgroundImage:[UIImage imageNamed:@"SVleftButton.png"] forState:UIControlStateNormal];
        [sixXSeven setBackgroundImage:[UIImage imageNamed:@"SVrightButtonEmpty.png"] forState:UIControlStateNormal];
        [beginGame setTitle:@" Start Game " forState:UIControlStateNormal];
        [beginGame setTitleColor:textColor forState:UIControlStateNormal];
        [fiveXSix setTitleColor:textColor forState:UIControlStateNormal];
        [sixXSeven setTitleColor:textColor forState:UIControlStateNormal];

    }
// Code to continue game - possible feature addition down the road.
//    if(gameBoard.alreadyStartedGame && (gData.p1 != NULL))
//    {
//        UIAlertView *continueGame = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Resume Game\n [ %@ vs %@ ]", gData.p1, gData.p2]                                                                                                                         
//                                                               message:nil
//                                                              delegate:self
//                                                     cancelButtonTitle:@"Yes" 
//                                                     otherButtonTitles:@"No", nil];
//        [continueGame show];
//        
//    }
//
//    if(gameBoard.playAgain)
//    {
//        [self reloadInputViews];
//        p1label.text = gameBoard.player1;
//        p2label.text = gameBoard.player2;
//        if(gameBoard.xDimension == 7)
//        {
//            gameBoard.xDimension = 7;
//            gameBoard.yDimension = 8;      
//        }
//        else if(gameBoard.xDimension == 5)
//        {
//            gameBoard.xDimension = 5;
//            gameBoard.yDimension = 6;
//        }
//    }
    else
    {
        p1label.text = nil;
        p2label.text = nil;
    }
    if( (gameBoard.player1 != NULL) && ( gameBoard.player2 != NULL))
    {
        
        [beginGame setBackgroundImage:[UIImage imageNamed:@"SVstartGame.png"] forState:UIControlStateNormal];
        [beginGame setBackgroundImage:[UIImage imageNamed:@"SVstartGameSelected.png"] forState:UIControlStateHighlighted];
        [beginGame setTitle:@" Start Game " forState:UIControlStateNormal];
        beginGame.enabled = TRUE;
        beginGame.alpha = 1.0;
    }
    
    UINavigationBar *navBar = self.navigationController.navigationBar;
    [navBar clearsContextBeforeDrawing];
    [navBar setBackgroundImage:[UIImage imageNamed:@"header.png"] forBarMetrics:UIBarMetricsDefault];
    
    UIImage *backImage = [UIImage imageNamed:@"backIconBrown.png"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 0, 40, 40);
    
    [backButton setImage:backImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(hideKeyboard:) forControlEvents:UIControlEventTouchDown];
    [backButton addTarget:self action:@selector(pushBackButton:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = backBarButtonItem;
    [gameBoard assignGameDataID:gameBoard];
}


-(void)pushBackButton:(id)sender
{
    [animation setDelegate:self];
    [animation setDuration:0.5];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    animation.type = @"pageUnCurl";
    
    animation.subtype = kCATransitionFromBottom;
    animation.fillMode = kCAFillModeBackwards;
    animation.startProgress = 0;
    [animation setRemovedOnCompletion:YES];
    
    [self.navigationController.view.layer addAnimation:animation forKey:@"pageCurlAnimation"];
    [self.navigationController popViewControllerAnimated:NO];
}
- (void)viewDidLoad
{
    if (![@"1" isEqualToString:[[NSUserDefaults standardUserDefaults]
                                objectForKey:@"Avalue"]]) {
        
        alert = [BlockAlertView alertWithTitle:@"Hint" message:@"\nTo play against the computer tap the pencil button just right of the player name."];
        [alert setDestructiveButtonWithTitle:@"Dismiss" block:nil];
        [alert show];
    }

    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

/**************************************************************************************
 * Purpose: IBAction used to slide the view up when the keyboard animations slide it up
 *
 * Reference: n/a
 **************************************************************************************/

-(IBAction)userBeganEnteringText:(id)sender
{
    [(UITextView *)sender setFont:sFont];
    float offset = 0;
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.25];

    //When user begins entering text set the view to recognize any gesture
    //when it does, call hideKeyboard -> textFieldShouldReturn
    if(SECONDVCPRINT)
        NSLog(@"User Began Entering Text\n");
     [self.view addGestureRecognizer:gestureRecognizer];
    
    //Hardcoded depending on label's location. Not a very elegant solution
    //but for the meantime it will have to work.
    if(sender == p1label)
        offset = CENTER.y - 50.0 ;
    else
        offset = CENTER.y - 110;
    if(!release_offset)
        CENTER.y = self.view.center.y;
    release_offset++;
    self.view.center = CGPointMake(self.view.center.x, offset);
    [UIView commitAnimations];

}

/**************************************************************************************
 * Purpose: IBAction used to get the value from the segmented button specifying grid size.
 *
 * Reference: n/a
 **************************************************************************************/

- (IBAction)setGridSize:(id)sender
{
    UIButton *temp = (UIButton *)sender;
    if(SECONDVCPRINT)
        NSLog(@"set grid size - gameBoard.reload = %d\n", gameBoard.reload);
    if(!gameBoard.reload)
    {
        if(temp.tag == 0) // 5x6  - set highlighted image
        {
            [gameBoard setXDimension:5];
            [gameBoard setYDimension:6];
            [fiveXSix setBackgroundImage:[UIImage imageNamed:@"SVleftButton.png"] forState:UIControlStateNormal];
            [fiveXSix setBackgroundImage:[UIImage imageNamed:@"SVleftButtonSelected.png"] forState:UIControlStateHighlighted];
            [sixXSeven setBackgroundImage:[UIImage imageNamed:@"SVrightButtonEmpty.png"] forState:UIControlStateNormal];
        }
        else if(temp.tag == 1)
        {
            [gameBoard setXDimension:7];
            [gameBoard setYDimension:8];
            [fiveXSix setBackgroundImage:[UIImage imageNamed:@"SVleftButtonEmpty.png"] forState:UIControlStateNormal];
            [sixXSeven setBackgroundImage:[UIImage imageNamed:@"SVrightButton.png"] forState:UIControlStateNormal];
            [sixXSeven setBackgroundImage:[UIImage imageNamed:@"SVrightButtonSelected.png"] forState:UIControlStateHighlighted];
        }
        if(SECONDVCPRINT)
            NSLog(@"gameBoard.xDimension = %d   gameBoard.yDimension = %d", [(gameLogic *)gameBoard xDimension], [(gameLogic *)gameBoard yDimension]);
    }
}

-(void)hideKeyboard
{
    [self textFieldShouldReturn:p1label];
    [self textFieldShouldReturn:p2label];
}

-(void)hideKeyboard:(id)sender
{
    [self textFieldShouldReturn:p1label];
    [self textFieldShouldReturn:p2label];
}

- (IBAction)doneEnteringText:(id)sender
{
    if(SECONDVCPRINT)
        NSLog(@"User Ended\n");
     [self textFieldShouldReturn:sender];
}

- (IBAction)GameOptions:(id)sender
{
    __block UIButton *sendingButton = (UIButton*)sender;
    __block UITextField *label;
    __block UITextField *otherlabel;
    __block UIButton *button;
    __block int flag;
    __weak secondViewController *weakself = self;
    if(sendingButton.tag == 0)
    {
        label = p1label;
        otherlabel = p2label;
        button = player1Check;
        flag = 1;
    }
    else
    {
        label = p2label;
        otherlabel = p1label;
        button = player2Check;
        flag = 2;
    }

    alert = [BlockAlertView alertWithTitle:@"Options" message:@"\nWhat would you like to set this player to?"];
    [alert setCancelButtonWithTitle:@"Computer" block:^{
        if(AIFlag && AIFlag != flag)
        {
            if(SECONDVCPRINT)
                NSLog(@"swap");
            NSString *str = label.text;
            label.text = otherlabel.text;
            otherlabel.text = str;
            otherlabel.enabled = TRUE;
            AIFlag = flag;
        }
        else
        {
            if(SECONDVCPRINT)
                NSLog(@"BoxBot");
            label.text = @"BoxBot";
            label.enabled = FALSE;
            AIFlag = flag;
        }
        [weakself checkIfReadyToBegin];

    }];
    [alert setDestructiveButtonWithTitle:@"Human" block:^{
        label.text = @"";
        label.enabled = TRUE;
        [button setBackgroundImage:[UIImage imageNamed:@"SVfirstPlayerRed.png"] forState:UIControlStateNormal];
        [weakself checkIfReadyToBegin];
        AIFlag = 0;
    }];
    
    [weakself textFieldShouldReturn:label];
    [alert show];
    
}

/**************************************************************************************
 * Purpose: Parse user's name and get the first character of it to use as thier initial
 * during the game. If initials are the same append 1 to first player's initial and 2
 * to the second player's, i.e. Nick & Nick -> N1 & N2
 *
 * Reference: n/a
 **************************************************************************************/

-(void)setInitials:(NSString *)p1 and:(NSString *)p2
{
    unichar p1Initial = [[p1 uppercaseString] characterAtIndex:0];
    unichar p2Initial = [[p2 uppercaseString] characterAtIndex:0];
    
    gameBoard.p1Initials  = [[NSMutableString alloc]init];
    gameBoard.p2Initials = [[NSMutableString alloc]init];
    [gameBoard.p2Initials appendFormat:@"%c", p2Initial];
    [gameBoard.p1Initials appendFormat:@"%c", p1Initial];
    
    //Handling same chars
    if( [gameBoard.p1Initials isEqualToString:gameBoard.p2Initials])
    {
        unichar p1append = '1';
        unichar p2append = '2';
        [gameBoard.p1Initials appendFormat:@"%c", p1append];
        [gameBoard.p2Initials appendFormat:@"%c", p2append];
    }

}
     
/**************************************************************************************
 * Purpose: Text field returning, remember to uncomment code to set the alpha of begin
 * game button as well as enable it. 
 *
 * Reference: n/a
 **************************************************************************************/

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
     if(textField == p1label)
         [gameBoard setPlayer1:p1label.text];
     else
         [gameBoard setPlayer2:p2label.text];
    
    [textField resignFirstResponder];
    [self checkIfReadyToBegin];
    [self.view removeGestureRecognizer:gestureRecognizer];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.25];
    if(release_offset)
        self.view.center = CGPointMake(CENTER.x, CENTER.y - y_center );
    release_offset = 0;
    [UIView commitAnimations];
    
    return TRUE;
}

-(void)checkIfReadyToBegin
{
    if(AIFlag == 1)
    {
        p1label.enabled = FALSE;
        p2label.enabled = TRUE;
    }
    else if (AIFlag == 2)
    {
        p2label.enabled = FALSE;
        p1label.enabled = TRUE;
    }
    gameBoard.player1 = p1label.text;
    gameBoard.player2 = p2label.text;
    
    if( (gameBoard.player1 != NULL) && ( gameBoard.player2 != NULL) && (![gameBoard.player1 isEqualToString:@""]) && (![gameBoard.player2 isEqualToString:@""]) && (p1label.text != NULL) && (p2label.text != NULL))
    {
        beginGame.enabled = TRUE;
        beginGame.alpha = 1.0;
        [beginGame setTitle:@" Start Game " forState:UIControlStateNormal];
        [beginGame setBackgroundImage:[UIImage imageNamed:@"SVstartGame.png"] forState:UIControlStateNormal];
        [beginGame setBackgroundImage:[UIImage imageNamed:@"SVstartGameSelected.png"] forState:UIControlStateHighlighted];
    }
    else
    {
        beginGame.enabled = FALSE;
        beginGame.alpha = .6;
    }
    if(gameBoard.player1.length > 0 || p1label.text.length > 0)
    {
        [player1Check setBackgroundImage:[UIImage imageNamed:@"SVfirstPlayerGreen.png"] forState:UIControlStateNormal];
    }
    else {
        [player1Check setBackgroundImage:[UIImage imageNamed:@"SVfirstPlayerRed.png"] forState:UIControlStateNormal];
    }
    
    if(gameBoard.player2.length > 0 || p2label.text.length > 0)
    {
        [player2Check setBackgroundImage:[UIImage imageNamed:@"SVfirstPlayerGreen.png"] forState:UIControlStateNormal];
    }
    else
    {
        [player2Check setBackgroundImage:[UIImage imageNamed:@"SVfirstPlayerRed.png"] forState:UIControlStateNormal];
        
    }
    
}


/**************************************************************************************
 * Purpose: When user has selected size of game board and has entered two user names
 * the game is ready to begin, push next viewController onto the stack.
 *
 * Reference: n/a
 **************************************************************************************/

-(IBAction)beginGame:(id)sender
{
    [self setInitials:gameBoard.player1 and:gameBoard.player2];
    if(SECONDVCPRINT)
        NSLog(@"gameBoard.p1Initial = %@    gameBoard.p2Initial = %@\n", [gameBoard p1Initials],  [gameBoard p2Initials] );
    gameBoard.boxbotflag = AIFlag;
    gameController = [[gameBoardViewController alloc]initWithNibName:@"gameBoardViewController" bundle:nil andBoard:gameBoard andData:gData];
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = nil;

    [self.navigationController setToolbarHidden:NO];
    [self.navigationController setNavigationBarHidden:FALSE];
    [animation setDelegate:self];
    [animation setDuration:0.6];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    animation.type = @"pageCurl";
    
    animation.subtype = kCATransitionFromTop;
    animation.fillMode = kCAFillModeForwards;
    animation.startProgress = 0;
    [animation setRemovedOnCompletion:YES];
    
    [self.navigationController.view.layer addAnimation:animation forKey:@"pageCurlAnimation"];
    [self.navigationController pushViewController:gameController animated:NO];
}

@end
