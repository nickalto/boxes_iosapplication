//
//  main.m
//  Boxes_iOSApplication
//
//  Created by NICK ALTO on 1/30/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Boxes_iOSApplicationAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([Boxes_iOSApplicationAppDelegate class]));
    }
}
