//
//  Boxes_iOSApplicationAppDelegate.h
//  Boxes_iOSApplication
//
//  Created by NICK ALTO on 1/30/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "firstViewController.h"
#import "customUINavigationController.h"

@interface Boxes_iOSApplicationAppDelegate : UIResponder <UIApplicationDelegate>
{
    customUINavigationController *navController;    // customNavigation Controller to stack viewControllers onto
    gameLogic *gameBoard;                           // create instance of gameBoard and pass it around
    gameData *gameBoardData;                        // Used to store game data and import it into the gameBoard
    NSFileManager *fm;                              // file manager used to save context
    int flag;
}

//Synthesized Variables
@property customUINavigationController *navController;
@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) gameLogic *gameBoard;
@property (nonatomic, strong) gameData *gameBoardData;
@property (nonatomic, strong) NSFileManager *fm;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (atomic) int flag;

//Public Instance Methods
- (void)copyData:(gameData*)gData game:(gameLogic *)gBoard;
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
- (void)saveData;
- (void)restoreData;
@end
