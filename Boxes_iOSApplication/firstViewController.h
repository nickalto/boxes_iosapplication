//
//  firstViewController.h
//  Boxes_iOSApplication
//
//  Created by NICK ALTO on 1/30/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h> 
#import "gameOptionViewController.h"
#import "gameData.h"


@interface firstViewController : UIViewController <UIAlertViewDelegate>
{
    IBOutlet UIButton *localGameButton;     // button to launch local pass-n-play game
    IBOutlet UIButton *onlineGameButton;    // butotn to launch gameCenter-backed game
    gameLogic *gameBoard;                   // gamelogic instance, all game data
    gameData *gData;
    IBOutlet UIButton *instructionButton;
}

//Synthesized Variables
@property (nonatomic, strong) gameLogic *gameBoard;
@property (nonatomic, strong) gameData *gData;

//Public Instance Methods
- (IBAction) beginOnlineGame: (id) sender;
- (IBAction) beginLocalGame: (id) sender;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andBoard:(id)game andData:(gameData *)data;

@end
