//
//  BlockUI.h
//
//  Created by Gustavo Ambrozio on 14/2/12.
//

#ifndef BlockUI_h
#define BlockUI_h

// Action Sheet constants

#define kActionSheetBounce         10
#define kActionSheetBorder         10
#define kActionSheetButtonHeight   45
#define kActionSheetTopMargin      15

#define kActionSheetTitleFont           [UIFont systemFontOfSize:18]
#define kActionSheetTitleTextColor      [UIColor clearColor]
#define kActionSheetTitleShadowColor    [UIColor blackColor]
#define kActionSheetTitleShadowOffset   CGSizeMake(0, -1)

#define kActionSheetButtonFont          [UIFont boldSystemFontOfSize:20]
#define kActionSheetButtonTextColor     [UIColor clearColor]
#define kActionSheetButtonShadowColor   [UIColor blackColor]
#define kActionSheetButtonShadowOffset  CGSizeMake(0, -1)

#define kActionSheetBackground              @"action-sheet-panel.png"
#define kActionSheetBackgroundCapHeight     30


// Alert View constants

#define kAlertViewBounce         20
#define kAlertViewBorder         2
#define kAlertButtonHeight       44

#define kAlertViewTitleFont             [UIFont fontWithName:@"FFFTusj-Bold"size:22]

#define kAlertViewTitleTextColor        [UIColor colorWithRed:.2509 green:.2157 blue:.1843 alpha:1]
#define kAlertViewTitleShadowColor      [UIColor clearColor]
#define kAlertViewTitleShadowOffset     CGSizeMake(0, -1)

#define kAlertViewMessageFont            [UIFont fontWithName:@"FFFTusj-Bold"size:16]
#define kAlertViewMessageTextColor      [UIColor colorWithRed:.2509 green:.2157 blue:.1843 alpha:1]
#define kAlertViewMessageShadowColor    [UIColor clearColor]
#define kAlertViewMessageShadowOffset   CGSizeMake(0, -1)

#define kAlertViewButtonFont             [UIFont fontWithName:@"FFFTusj-Bold"size:18]
#define kAlertViewButtonTextColor       [UIColor colorWithRed:.2509 green:.2157 blue:.1843 alpha:1]
#define kAlertViewButtonShadowOffset    CGSizeMake(0, -1)
#define kAlertViewButtonSelected       [UIColor colorWithRed:.3509 green:.3157 blue:.1843 alpha:1]


#define kAlertViewBackground            @"alert-window.png"
#define kAlertViewBackgroundCapHeight   38

#endif
