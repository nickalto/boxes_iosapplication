//
//  Boxes_iOSApplicationAppDelegate.m
//  Boxes_iOSApplication
//
//  Created by NICK ALTO on 1/30/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "Boxes_iOSApplicationAppDelegate.h"
BOOL DELEGATEPRINTING = FALSE;
@interface Boxes_iOSApplicationAppDelegate()
{
    bool previousGame;                              // bool denoting a previous game has been played
    NSString *moifile;                              // NSString used to store game data when terminated
    NSString *docs;                                 // Same use as moifile
}

@end
@implementation Boxes_iOSApplicationAppDelegate

@synthesize window                      = _window;
@synthesize managedObjectContext        = __managedObjectContext;
@synthesize managedObjectModel          = __managedObjectModel;
@synthesize persistentStoreCoordinator  = __persistentStoreCoordinator;
@synthesize gameBoard                   = _gameBoard;
@synthesize gameBoardData               = _gameBoardData;
@synthesize fm                          = _fm;
@synthesize navController               = _navController;
@synthesize flag;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    if(DELEGATEPRINTING)
    {
        if( getenv("NSZombieEnabled") || getenv("NSAutoreleaseFreedObjectCheckEnabled"))
        {
            if(DELEGATEPRINTING)
            NSLog(@"NSZombieEnabled/NSAutoreleaseFreedObjectCheckEnabled enabled!");
        }
    }
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    //create navigation controller to push view controller on to
     navController = [[customUINavigationController alloc]init];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    docs = [paths  objectAtIndex:0];
    fm = [[NSFileManager alloc]init];
    NSString *temp = [docs stringByAppendingFormat:@"/12Dot"];
    NSError *err;
    BOOL exists = [fm fileExistsAtPath:temp];
    if(!exists)
    {
        err = nil;
        [fm createDirectoryAtPath:temp  withIntermediateDirectories:NO attributes:nil error:&err];
    }

    gameBoardData = [[gameData alloc]init];
    gameBoard = [[gameLogic alloc]init];
    
    [self restoreData];

    navController.navigationBar.tintColor = [UIColor whiteColor];
    firstViewController *firstView = [[firstViewController alloc]initWithNibName:@"firstViewController" bundle:nil andBoard:gameBoard andData:gameBoardData];
    [navController pushViewController:firstView animated:YES];
    [self.window addSubview:navController.view];
    
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"coldBoot"
     object:gameBoard];
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
       [self copyData:gameBoardData game:gameBoard];
    
    if(!(gameBoard.player1 == NULL) || !(gameBoard.xDimension == 0))
    {
        if(!(gameBoard.reload == -10) && (![gameBoard.player1 isEqualToString:@"(null)"]) && (gameBoard.p1Score != 1000))
        {
            if(DELEGATEPRINTING) {
                NSLog(@"saved from applicationDidEnterBackground\n");
                NSLog(@"gameboard.player1 = %@, gameboard.player2 = %@\n", gameBoard.player1, gameBoard.player2);
            }
            [self saveData];
        }
    }
    if((gameBoard.reload == -10)  && (gameBoard.p1Score != 1000))
    {
        NSError *err;
        [fm createDirectoryAtPath:docs withIntermediateDirectories:NO attributes:nil error:&err];
        NSString *myFolder; 
        
        if(gameBoard.onlineGame == 0)
            myFolder = [docs stringByAppendingFormat:@"/12Dot/Local"];
        else if(gameBoard.onlineGame == 1)
            myFolder = [docs stringByAppendingFormat:@"/12Dot/Online"];
        
        BOOL exists = [fm fileExistsAtPath:myFolder];
        if(DELEGATEPRINTING)
        NSLog(@"Data exists at %@: %d", myFolder, [[NSNumber numberWithBool:exists]intValue]);
        
        if(!exists)
        {
            err = nil;
            [fm createDirectoryAtPath:myFolder withIntermediateDirectories:NO attributes:nil error:&err];
        }
        
        moifile = [myFolder stringByAppendingPathComponent:[NSString stringWithFormat:@"gameData%d", gameBoard.gameDataID]];

        exists = [fm fileExistsAtPath:moifile];
        if(exists)
            [fm removeItemAtPath:moifile error:&err];
    }


    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

/**************************************************************************************
 * Purpose: Called everytime the application did enter the background, and saves all
 * important game state from the gameLogic instance to the gameData instance.
 *
 * Reference: Called only from the Boxes Delegate when the game backgrounded.
 **************************************************************************************/

-(void)copyData:(gameData*)gData game:(gameLogic *)gBoard
{
    int i = 0;

    gData.xLineArray        = [NSMutableArray arrayWithArray:gBoard.xLineArr];
    gData.yLineArray        = [NSMutableArray arrayWithArray:gBoard.yLineArr];
    gData.initialsCGRect    = [NSMutableArray arrayWithArray:gBoard.arrOfLabels];
    gData.initialsPlayer    = [NSMutableArray arrayWithArray:gBoard.arrHelper];
    gData.p1                = gBoard.player1;
    gData.p1score           = [NSNumber numberWithInt:gBoard.p1Score];
    gData.p1initials        = gBoard.p1Initials;
    gData.p2                = gBoard.player2;
    gData.p2score           = [NSNumber numberWithInt:gBoard.p2Score];
    gData.p2initials        = gBoard.p2Initials;
    gData.turn              = [NSNumber numberWithInt:gBoard.playerTurn];
    gData.xDim              = [NSNumber numberWithInt:gBoard.xDimension];
    gData.yDim              = [NSNumber numberWithInt:gBoard.yDimension];
    gData.gameDataID        = [NSNumber numberWithInt:gBoard.gameDataID];

    
    if(DELEGATEPRINTING)
    {
    
        // print out data to make sure xLineArray data is correct
        for(i = 0; i < ((gBoard.xDimension - 1) * gBoard.yDimension); i++)
            NSLog(@" %d -- %d\n", i, [[gData.xLineArray objectAtIndex:i]intValue]);
        
        // print out yLineArray data to make sure it's correct
        for(i = 0; i < (gBoard.xDimension  * (gBoard.yDimension - 1)); i++)
            NSLog(@" %d -- %d\n", i, [[gData.yLineArray objectAtIndex:i]intValue]);
        
        // check initialsCGRect values and intialsPlayer
        for(i = 0; i < gBoard.p1Score + gBoard.p2Score; i++)
        {
            CGRect a = [[gData.initialsCGRect objectAtIndex:i]CGRectValue];
            NSLog(@" Player  = %d --- %f, %f\n",[[gData.initialsPlayer objectAtIndex:i]intValue],a.origin.x, a.origin.y  );
        }
        

        NSLog(@"%@  %d  %@\n", gData.p1, [gData.p1score intValue], gData.p1initials );
        NSLog(@"%@  %d  %@\n", gData.p2, [gData.p2score intValue], gData.p2initials);
        NSLog(@"%d  %d x %d\n", [gData.turn intValue], [gData.xDim intValue], [gData.yDim intValue]);
        
    }
}


-(void)deleteData
{
    NSError *err;
    [fm createDirectoryAtPath:docs withIntermediateDirectories:NO attributes:nil error:&err];
    NSString *myFolder = [docs stringByAppendingFormat:@"/12Dot"];
    BOOL exists = [fm fileExistsAtPath:myFolder];    
    moifile = [myFolder stringByAppendingPathComponent:@"gameData"];
    exists = [fm fileExistsAtPath:moifile];
    if(exists)
        [fm removeItemAtPath:moifile error:&err];
}

/**************************************************************************************
 * Purpose: Saves the gameData instance to file so it can be reloaded from a fresh
 * start. 
 *
 * Reference: Called right after copyData from applicationDidEnterBackground
 **************************************************************************************/

-(void)saveData
{
    if(gameBoard.player1.length > 0 && gameBoard.player2.length > 0)
    {
        NSError *err;
        [fm createDirectoryAtPath:docs withIntermediateDirectories:NO attributes:nil error:&err];
        NSString *myFolder; 
        
        if(gameBoard.onlineGame == 0)
            myFolder = [docs stringByAppendingFormat:@"/12Dot/Local"];
        else if(gameBoard.onlineGame == 1)
            myFolder = [docs stringByAppendingFormat:@"/12Dot/Online"];
        
        BOOL exists = [fm fileExistsAtPath:myFolder];
        if(DELEGATEPRINTING)
        NSLog(@"Data exists at %@: %d", myFolder, [[NSNumber numberWithBool:exists]intValue]);
        
        if(!exists)
        {
            err = nil;
            if(DELEGATEPRINTING)
            NSLog(@"creating directory myfolder= %@\n", myFolder);
            [fm createDirectoryAtPath:myFolder withIntermediateDirectories:NO attributes:nil error:&err];
        }
        NSData *moidata = [NSKeyedArchiver archivedDataWithRootObject:gameBoardData];
        
        moifile = [myFolder stringByAppendingPathComponent:[NSString stringWithFormat:@"gameData%d", gameBoard.gameDataID]];
        
        exists = [fm fileExistsAtPath:moifile];
        if(exists)
            [fm removeItemAtPath:moifile error:&err];
        
        [moidata writeToFile:moifile atomically:YES];
    }
    
}

/**************************************************************************************
 * Purpose: Check to see if there is data saved at a given location, if so retrieve it
 * and save that data into the gameData instance. Prompt the user in the next view
 * firstViewController if they want to restore from that data and continue playing.
 *
 * Reference: Called right after allocating gameLogic and gameData instances in 
 * applicationdidFinishLaunchingWithOptions at top of the file
 **************************************************************************************/

-(void)restoreData
{
    //NSError *err;
    NSString *myFolder;
    
    if(gameBoard.onlineGame == 0)
        myFolder = [docs stringByAppendingFormat:@"/12Dot/Local"];
    else if(gameBoard.onlineGame == 1)
        myFolder = [docs stringByAppendingFormat:@"/12Dot/Online"];
    
    BOOL exists = [fm fileExistsAtPath:myFolder];
    if(DELEGATEPRINTING)
    NSLog(@"Data exists at %@: %d", myFolder, [[NSNumber numberWithBool:exists]intValue]);

    moifile = [myFolder stringByAppendingPathComponent:[NSString stringWithFormat:@"gameData%d", gameBoard.gameDataID]];
    
    exists = [fm fileExistsAtPath:moifile];
    if(DELEGATEPRINTING)
    NSLog(@"Data exists at %@: %d", moifile, [[NSNumber numberWithBool:exists]intValue]);
    NSData *data = [[NSData alloc]initWithContentsOfFile:moifile];
    if(data != NULL)
    {
        gameBoard.alreadyStartedGame = 1;
        gameBoardData = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        if(DELEGATEPRINTING)
        {
            
            NSLog(@"exists p1 = %@  p2 = %@\n", gameBoardData.p1, gameBoardData.p2);
                int i = 0;

                // print out data to make sure xLineArray data is correct
                for(i = 0; i < (([gameBoardData.xDim intValue] - 1) * [gameBoardData.yDim intValue]); i++)
                    NSLog(@" %d -- %d\n", i, [[gameBoardData.xLineArray objectAtIndex:i]intValue]);
                
                // print out yLineArray data to make sure it's correct
                for(i = 0; i < ([gameBoardData.xDim intValue]  * ([gameBoardData.yDim intValue] - 1)); i++)
                    NSLog(@" %d -- %d\n", i, [[gameBoardData.yLineArray objectAtIndex:i]intValue]);
                
                // check initialsCGRect values and intialsPlayer
                for(i = 0; i < [gameBoardData.p1score intValue] + [gameBoardData.p2score intValue]; i++)
                {
                    CGRect a = [[gameBoardData.initialsCGRect objectAtIndex:i]CGRectValue];
                    NSLog(@" Player  = %d --- %f, %f\n",[[gameBoardData.initialsPlayer objectAtIndex:i]intValue],a.origin.x, a.origin.y  );
                }
                
                NSLog(@"%@  %d  %@\n", gameBoardData.p1, [gameBoardData.p1score intValue], gameBoardData.p1initials );
                NSLog(@"%@  %d  %@\n", gameBoardData.p2, [gameBoardData.p2score intValue], gameBoardData.p2initials);
                NSLog(@"%d  %d x %d\n", [gameBoardData.turn intValue], [gameBoardData.xDim intValue], [gameBoardData.yDim intValue]);
             
        }
    }

    else if(data == NULL || !exists)
    {
        gameBoard.alreadyStartedGame = 0;
        if(DELEGATEPRINTING)
        NSLog(@"doesn't exist new data\n");
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    if(gameBoard.reload == -10)
        [self deleteData];
    gameBoard.refreshBoard = -10;
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    gameBoard.refreshBoard = 1;
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"coldBoot"
     object:gameBoard];
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}


- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil)
    {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error])
        {
            /*
             Replace this implementation with code to handle the error appropriately.
             
             abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
             */
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
    }
}

#pragma mark - Core Data stack

/**
 Returns the managed object context for the application.
 If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
 */
- (NSManagedObjectContext *)managedObjectContext
{
    if (__managedObjectContext != nil)
    {
        return __managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil)
    {
        __managedObjectContext = [[NSManagedObjectContext alloc] init];
        [__managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return __managedObjectContext;
}

/**
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created from the application's model.
 */
- (NSManagedObjectModel *)managedObjectModel
{
    if (__managedObjectModel != nil)
    {
        return __managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Boxes_iOSApplication" withExtension:@"momd"];
    __managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];    
    return __managedObjectModel;
}

/**
 Returns the persistent store coordinator for the application.
 If the coordinator doesn't already exist, it is created and the application's store added to it.
 */
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (__persistentStoreCoordinator != nil)
    {
        return __persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Boxes_iOSApplication.sqlite"];
    
    NSError *error = nil;
    __persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![__persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error])
    {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter: 
         [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }    
    
    return __persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

/**
 Returns the URL to the application's Documents directory.
 */
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end

