//
//  onlineonlineGameBoardViewController.h
//  Boxes_iOSApplication
//
//  Created by NICK ALTO on 6/20/12.
//  Copyright (c) 2012 Oregon State University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "gameLogic.h"
#import "customUIView.h"
#import <QuartzCore/QuartzCore.h> 
#import "gameData.h"
#import "BlockAlertView.h"


@interface onlineGameBoardViewController : UIViewController <UIScrollViewDelegate, UIAlertViewDelegate>
{
    gameLogic *gameBoard;                   // gameboard instance
    gameData *data;
    IBOutlet UIScrollView * scrollView;     // scrollview to easily navigate customUIView
    IBOutlet customUIView *contentView;     // customUIView where the game will occur
    UIGestureRecognizer *pinchGesture;      // gesture recognizer for zoom gesture
    IBOutlet UIButton *submitScore;
    IBOutlet UIButton *rightHome;
    IBOutlet UIButton *leftHome;
    
}

//Synthesized Variables
@property (nonatomic, strong) gameLogic *gameBoard;
@property (nonatomic, strong) customUIView* contentView;

//Public Instance Methods
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andBoard:(id)game andData:(gameData *)gData;
- (void)initializeLines:(gameLogic *)game andFlag:(BOOL)flag;
@end
