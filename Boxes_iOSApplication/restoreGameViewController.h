//
//  restoreGameViewController.h
//  Boxes_iOSApplication
//
//  Created by NICK ALTO on 4/14/12.
//  Copyright (c) 2012 Oregon State University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h> 
#import "gameData.h"
#import "gameLogic.h"
#import "gameBoardViewController.h"
#import "BlockAlertView.h"

@interface restoreGameViewController : UIViewController
{
    gameLogic *gameBoard;                   // gamelogic instance, all game data
    gameData *gData;
    gameBoardViewController *gameController;
    restoreGameViewController *nextVC;
    int controllerID;
}

//Synthesized Variables
@property (nonatomic, strong) gameLogic *gameBoard;
@property (nonatomic, strong) gameData *gData;

- (IBAction) backButton:(id)sender;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andBoard:(id)game andData:(gameData *)data ID:(int)viewID;
-(gameData *)getGame:(int) i del:(int)delete;


@end
