//
//  Dot.h
//  Boxes_iOSApplication
//
//  Created by NICK ALTO on 1/30/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Dot : NSObject
{
    int dotID;                  // Used to uniquely identify each dot
    CGPoint dotLocation;        // Log's each dot's location in the custom view
    id gameBoard;               // Each dot has access to it's gameboard 
    CGPoint endLocation;        // CGPoint of final dot touched
    int up;                     // if a given dot has a dot above 
    int down;                   // if a given dot has a dot below 
    int right;                  // if a given dot has a dot to the right of it
    int left;                   // if a given dot has a dot to the left of it
    int xDim;                   // dimension of the gameboard
    UIButton *dotButton;
}

//Synthesized Variables
@property CGPoint endLocation;
@property (nonatomic, strong) id gameBoard;
@property int xDim;
@property int up;
@property (nonatomic, strong) UIButton *dotButton;
@property int down;
@property int right;
@property int left;
@property int dotID;
@property CGPoint dotLocation;

//Public Instance Methods
-(id)initWithID:(int)did;

@end
