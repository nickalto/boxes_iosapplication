//
//  gameData.h
//  Boxes_iOSApplication
//
//  Created by NICK ALTO on 2/25/12.
//  Copyright (c) 2012 Oregon State University. All rights reserved.
//

#import <Foundation/Foundation.h>

/**************************************************************************************
 * Purpose: Modular class that acts as the instance of gameData for multiple games. The
 * idea is that this class will be called only from the Boxes Delegate and will save
 * crucial game state whenever the app is terminated, backgrounded, etc. So that there
 * will always be a new gameLogic instance whenever the game starts but the data will
 * be copied from this gameData class into the gameBoard and redrawn. This will make it
 * easier to manage online games as well, storing only 1 local game gameData instance
 * at a certain location on disk, and multiple online games. This will allow the user
 * to be able to select which online gameData instance to reload when playing online 
 * games allowing for multiple games to played. 
 *
 * Reference: Called only from the Boxes Delegate when the game is started and when it 
 * gets terminated. 
 **************************************************************************************/
#define IS_IPHONE5 (([[UIScreen mainScreen] bounds].size.height-568)?NO:YES)


@interface gameData : NSObject <NSCoding>
{  
    int dataID;                             // Used to Uniquely identify data instances;
    NSMutableArray *xLineArray;             // Used to store xLineArr from gameLogic
    NSMutableArray *yLineArray;             // Used to store yLineArr from gameLogic
    NSMutableArray *initialsCGRect;         // Label rect for redrawing won boxes
    NSMutableArray *initialsPlayer;         // array corresponding to the array of labels - specifying the box
    NSString *p1;                           // first player's name
    NSString *p2;                           // second player's name
    NSNumber *p1score;                      // first player's score
    NSNumber *p2score;                      // second player's score
    NSString *p1initials;                   // first player's initials
    NSString *p2initials;                   // second player's initials
    NSNumber *turn;                         // designates who's turn it is 1 = p1 - 2 = p2
    NSNumber *xDim;                         // x dimension of the gameboard
    NSNumber *yDim;                         // y dimension of the gameboard
    NSNumber *gameDataID;
    NSNumber *boxbotflag;
}

//Synthesized Variables
@property (nonatomic, strong) NSMutableArray *xLineArray;
@property (nonatomic, strong) NSMutableArray *yLineArray;
@property (nonatomic, strong) NSMutableArray *initialsCGRect;
@property (nonatomic, strong) NSMutableArray *initialsPlayer;
@property (nonatomic, retain) NSString *p1;
@property (nonatomic, retain) NSString *p2; 
@property (nonatomic, retain) NSNumber *p1score;
@property (nonatomic, retain) NSNumber *p2score;
@property (nonatomic, retain) NSString *p1initials;
@property (nonatomic, retain) NSString *p2initials;
@property (nonatomic, retain) NSNumber *turn;
@property (nonatomic, retain) NSNumber *xDim;
@property (nonatomic, retain) NSNumber *yDim;
@property (nonatomic, retain) NSNumber *gameDataID;
@property (nonatomic, retain) NSNumber *boxbotflag;


//Public Instance Methods

@end
