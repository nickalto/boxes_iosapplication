//
//  InfoViewController.h
//  Boxes_iOSApplication
//
//  Created by NICK ALTO on 5/29/12.
//  Copyright (c) 2012 Oregon State University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "gameData.h"
#import <QuartzCore/QuartzCore.h> 

@interface InfoViewController : UIViewController
{
    IBOutlet UIButton* exit;
    IBOutlet UILabel *i1l1;
    IBOutlet UILabel *i1l2;
    IBOutlet UILabel *i1l3;
    IBOutlet UILabel *i1l4;
    IBOutlet UILabel *i1l5;
    IBOutlet UILabel *i1l6;
    IBOutlet UILabel *i1l7;
    IBOutlet UILabel *i1l8;
    IBOutlet UIImageView *i1g1;
    IBOutlet UIImageView *i1g2;

}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andInt:(int)instructionNumber andVC:(id)controller;

@end
