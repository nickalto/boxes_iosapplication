//
//  customUINavigationController.m
//  Boxes_iOSApplication
//
//  Created by NICK ALTO on 2/4/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "customUINavigationController.h"
BOOL CUSTOMNAVCONTROLLERPRINT = FALSE;
@implementation customUINavigationController
@synthesize framep1             = _framep1;
@synthesize framep2             = _framep2;
@synthesize framep1score        = _framep1score;
@synthesize framep2score        = _framep2score;
@synthesize turnFrame           = _turnFrame;

/**************************************************************************************
 * Purpose: Called to initialize a custom UINavigationController primairly to make a
 * custom navigation bar to house the player's names and scores. The init function 
 * initializes the labels and frames for the individual player names, scores, and turn
 * indicator as well as subscribes to a number of notifications.
 *
 * Reference: Called by gameBoardViewController
 **************************************************************************************/

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    //change each rectmake from [x, 5, x, x] to [x, 10, x, x] to accomodate font

    [self.navigationBar setBackgroundImage:[UIImage imageNamed:@"header.png"] forBarMetrics:UIBarMetricsDefault];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 400, 44)];
    label.font = [UIFont fontWithName:@"FFFTusj-Bold" size:32];
    label.text = @" 12Dot ";
    label.textAlignment = UITextAlignmentCenter;
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor grayColor];
    self.navigationItem.titleView = label;
    framep1 = CGRectMake(10, 10, 100, 30);
    framep1score = CGRectMake(115, 10, 30, 30);
    framep2score  = CGRectMake( 175, 10, 30, 30);
    framep2 = CGRectMake(200, 10, 100, 30);

    p1score = [[UILabel alloc]initWithFrame:framep1score];
    p1score.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    p1score.backgroundColor = [UIColor clearColor];
    p1score.textColor = [UIColor blackColor];
    p1score.textAlignment = UITextAlignmentRight;
    
    p1name = [[UILabel alloc]initWithFrame:framep1];
    p1name.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    p1name.backgroundColor = [UIColor clearColor];
    p1name.textColor = [UIColor blackColor];
    p1name.textAlignment = UITextAlignmentLeft;

    p2score = [[UILabel alloc]initWithFrame:framep2score];
    p2score.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    p2score.backgroundColor = [UIColor clearColor];
    p2score.textColor = [UIColor blackColor];
    p2score.textAlignment = UITextAlignmentLeft;

    p2name = [[UILabel alloc]initWithFrame:framep2];
    p2name.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    p2name.backgroundColor = [UIColor clearColor];
    p2name.textAlignment = UITextAlignmentRight;
    p2name.textColor = [UIColor blackColor];
    
//    CALayer *viewLayer = self.view.layer;
//    viewLayer.backgroundColor = [[UIColor clearColor] CGColor];
//    viewLayer.borderColor = [[UIColor blackColor] CGColor];
//    viewLayer.cornerRadius = 0;
//    viewLayer.borderWidth = 1.0f;

    //notification to change player's names
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(changeTitle:)
     name:@"changeTitle"
     object:nil];
    
    //notification to update p1 score
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(updateScoreP1:)
     name:@"updateScoreP1"
     object:nil];
    
    //notification to update p2 score
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(updateScoreP2:)
     name:@"updateScoreP2"
     object:nil];
    
    //notification to update turnIndicator
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(changeTurn:)
     name:@"changeTurn"
     object:nil];
    
    //notificatin to remove navBarInfo
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(removeTitle:)
     name:@"removeNavBarInfo"
     object:nil];
    

    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}
*/

/*-(void)removeNotifications:(NSNotification *)notification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"removeNotifications" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"updateScoreP2" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"changeTurn" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"removeNavBarInfo" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"changeTitle" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"updateScoreP1" object:nil];



}*/

/**************************************************************************************
 * Purpose: Notification method to change player's names, only called when creating a
 * new game in secondViewController
 *
 * Reference: Called by secondViewController
 **************************************************************************************/

-(void)changeTitle:(NSNotification *)notification
{
    gameLogic *gameBoard = [notification object];
    UIFont *dFont = [UIFont fontWithName:@"FFFTusj-Bold" size:18];

    [p1name setFont:dFont];
    [p2name setFont:dFont];
    [p1score setFont:dFont];
    [p2score setFont:dFont];
    p1name.text = gameBoard.player1;
    p1score.text = [NSString stringWithFormat:@"%d", gameBoard.p1Score];
    p2score.text = [NSString stringWithFormat:@"%d", gameBoard.p2Score];
    p2name.text = gameBoard.player2;
    
    [self.navigationBar addSubview:p2name];
    [self.navigationBar addSubview:p2score];
    [self.navigationBar addSubview:p1score];
    [self.navigationBar addSubview:p1name];
}

/**************************************************************************************
 * Purpose: Removes all data from UINavigationBar so that when a game ends or a player
 * quits the game the previous views don't have the game data still around in the nav
 * bar menu.
 *
 * Reference: Called by gameBoardViewController
 **************************************************************************************/

-(void)removeTitle:(NSNotification *)notification
{
    [p2name removeFromSuperview];
    [p2score removeFromSuperview];
    [p1score removeFromSuperview];
    [p1name removeFromSuperview];
    [self.navigationBar setBackgroundImage:[UIImage imageNamed:@"header.png"] forBarMetrics:UIBarMetricsDefault];
    //[turnIndicator removeFromSuperview];
}

/**************************************************************************************
 * Purpose: Updates player 1's score
 *
 * Reference: Called by gameLogic
 **************************************************************************************/

-(void)updateScoreP1:(NSNotification *)notification
{    
    gameLogic* passedBoard = [notification object];
    if(CUSTOMNAVCONTROLLERPRINT)
    {
        NSLog(@"recieved updateScoreP1 notification\n");
        NSLog(@"updated player 1 score = %d\n", passedBoard.p1Score);
    }
    UIFont *sFont = [UIFont fontWithName:@"FFFTusj-Bold" size:18];
    [p1score setFont:sFont];
     p1score.text = [NSString stringWithFormat:@"%d", passedBoard.p1Score];
}

/**************************************************************************************
 * Purpose: Updates player 2's score
 *
 * Reference: Called by gameLogic
 **************************************************************************************/

-(void)updateScoreP2:(NSNotification *)notification
{
    gameLogic* passedBoard = [notification object];
    if(CUSTOMNAVCONTROLLERPRINT)
    {
        NSLog(@"recieved updateScoreP2 notification\n");
        NSLog(@"updated player 2 score = %d\n", passedBoard.p2Score);
    }
    UIFont *sFont = [UIFont fontWithName:@"FFFTusj-Bold" size:18];
    [p2score setFont:sFont];
    p2score.text = [NSString stringWithFormat:@"%d", passedBoard.p2Score];
}

/**************************************************************************************
 * Purpose: Updates the turn indicator which currently is just a dot next to the player's 
 * name, in the future that might be upgraded for something a little more visually 
 * attrictive. 
 *
 * Reference: Called by gameLogic
 **************************************************************************************/
-(void)changeTurn:(NSNotification *)notification
{
    gameLogic* passedBoard = [notification object];
    if(passedBoard.playerTurn == 1)
    {
        [self.navigationBar setBackgroundImage:[UIImage imageNamed:@"leftHeader.png"] forBarMetrics:UIBarMetricsDefault];        
    }
    else if(passedBoard.playerTurn == 2)
    {
        [self.navigationBar setBackgroundImage:[UIImage imageNamed:@"rightHeader.png"] forBarMetrics:UIBarMetricsDefault];
    }
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
