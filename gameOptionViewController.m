//
//  gameOptionViewController.m
//  Boxes_iOSApplication
//
//  Created by NICK ALTO on 4/14/12.
//  Copyright (c) 2012 Oregon State University. All rights reserved.
//

#import "gameOptionViewController.h"
#import "InfoViewController.h"


@interface gameOptionViewController ()
{
    restoreGameViewController *restoreVC;
    BlockAlertView *alert;
    IBOutlet UIButton *instructionButton;
    CATransition *animation;

}
@end

@implementation gameOptionViewController
@synthesize gameBoard           = _gameBoard;
@synthesize gData               = _gData;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andBoard:(gameLogic *)game andData:(gameData *)data
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        gameBoard               = game;
        gData                   = data;
    }
    return self;
}


- (IBAction) pressedContinueGame: (id) sender
{
     restoreVC = [[restoreGameViewController alloc]initWithNibName:@"restoreGameViewController" bundle:nil andBoard:gameBoard andData:gData ID:1];
    [animation setDelegate:self];
    [animation setDuration:0.5];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    animation.type = @"pageCurl";
    
    animation.subtype = kCATransitionFromTop;
    animation.fillMode = kCAFillModeForwards;
    animation.startProgress = 0;
    [animation setRemovedOnCompletion:YES];
    
    [self.navigationController.view.layer addAnimation:animation forKey:@"pageCurlAnimation"];
    [self.navigationController pushViewController:restoreVC animated:NO];
    
}

- (IBAction) pressedNewGame: (id) sender
{
    if(!gameBoard.onlineGame)
    {
        if([gameBoard assignGameDataID:gameBoard] == -1)
        {
            alert = [BlockAlertView alertWithTitle:@"Out of Game Storage" message:@"\nPlease resume or delete a current local game"];
            [alert setCancelButtonWithTitle:@"Ok" block:nil];
            [alert show];

        }
        else {
        gameBoard.p1Score = 0;
        secondViewController *secondView = [[secondViewController alloc]initWithNibName:@"secondViewController" bundle:nil andBoard:gameBoard andData:gData];
        
        self.navigationController.view.autoresizesSubviews = FALSE;
            [animation setDelegate:self];
            [animation setDuration:0.5];
            [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
            animation.type = @"pageCurl";
            
            animation.subtype = kCATransitionFromTop;
            animation.fillMode = kCAFillModeForwards;
            animation.startProgress = 0;
            [animation setRemovedOnCompletion:YES];
            
            [self.navigationController.view.layer addAnimation:animation forKey:@"pageCurlAnimation"];
        [self.navigationController pushViewController:secondView animated:NO];
        }
    }
    else if(gameBoard.onlineGame)
    {
        alert = [BlockAlertView alertWithTitle:@"Select Game Size" message:nil];    

    }
}

- (void)viewWillAppear:(BOOL)animated
{
    if(IS_IPHONE5)
        [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"i5.png"]]];
    else
        [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"plainBackground.jpg"]]];
    
    animation = [CATransition animation];
    UINavigationBar *navBar = self.navigationController.navigationBar;
    [navBar clearsContextBeforeDrawing];
    [navBar setBackgroundImage:[UIImage imageNamed:@"header.png"] forBarMetrics:UIBarMetricsDefault];
    
    UIFont *sFont = [UIFont fontWithName:@"FFFTusj-Bold" size:24];

    newGame.titleLabel.textColor = [UIColor whiteColor];
    [newGame.titleLabel setFont:sFont];
    [newGame setTitle:@" New Game " forState:UIControlStateNormal];
    [newGame setTitleColor:[UIColor colorWithRed:.2509 green:.2157 blue:.1843 alpha:1] forState:UIControlStateNormal];
    [newGame setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
    [newGame setBackgroundImage:[UIImage imageNamed:@"FVbutton1Selected.png"] forState:UIControlStateHighlighted];
    [newGame setBackgroundImage:[UIImage imageNamed:@"FVbutton1.png"] forState:UIControlStateNormal];
    
    [continueGame.titleLabel setFont:sFont];
    [continueGame setTitle:@" Resume " forState:UIControlStateNormal];
    [continueGame setTitleColor:[UIColor colorWithRed:.2509 green:.2157 blue:.1843 alpha:1] forState:UIControlStateNormal];
    [continueGame setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
    [continueGame setBackgroundImage:[UIImage imageNamed:@"FVbutton2Selected.png"] forState:UIControlStateHighlighted];
    [continueGame setBackgroundImage:[UIImage imageNamed:@"FVbutton2.png"] forState:UIControlStateNormal];
    
    [instructionButton setBackgroundImage:[UIImage imageNamed:@"logo.png"] forState:UIControlStateNormal];
    [instructionButton setBackgroundImage:[UIImage imageNamed:@"logoSelected.png"] forState:UIControlStateHighlighted];
    
    UIImage *backImage = [UIImage imageNamed:@"backIconBrown.png"];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 0, 40, 40);
        
    [backButton setImage:backImage forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = backBarButtonItem;

    [super viewWillAppear:animated];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(IBAction)information:(id)sender
{
        InfoViewController *info = [[InfoViewController alloc]initWithNibName:@"InfoViewController" bundle:nil andInt:1 andVC:self];
        [animation setDelegate:self];
        [animation setDuration:0.5];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
        animation.type = @"pageUnCurl";
        
        animation.subtype = kCATransitionFromBottom;
        animation.fillMode = kCAFillModeBackwards;
        animation.startProgress = 0;
        [animation setRemovedOnCompletion:YES];
        
        [self.navigationController.view.layer addAnimation:animation forKey:@"pageCurlAnimation"];
        [self.navigationController pushViewController:info animated:NO];

}


-(void)backButton:(id)sender
{    
    [animation setDelegate:self];
    [animation setDuration:0.5];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault]];
    animation.type = @"pageUnCurl";
    
    animation.subtype = kCATransitionFromBottom;
    animation.fillMode = kCAFillModeBackwards;
    animation.startProgress = 0;
    [animation setRemovedOnCompletion:YES];
    
    [self.navigationController.view.layer addAnimation:animation forKey:@"pageCurlAnimation"];
    [self.navigationController popViewControllerAnimated:NO];

}

@end
