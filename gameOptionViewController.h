//
//  gameOptionViewController.h
//  Boxes_iOSApplication
//
//  Created by NICK ALTO on 4/14/12.
//  Copyright (c) 2012 Oregon State University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h> 
#import "restoreGameViewController.h"
#import "secondViewController.h"
#import "gameData.h"

@interface gameOptionViewController : UIViewController
{
    IBOutlet UIButton *continueGame;        // button to launch local pass-n-play game
    IBOutlet UIButton *newGame;             // butotn to launch gameCenter-backed game
    gameLogic *gameBoard;                   // gamelogic instance, all game data
    gameData *gData;
}


//Synthesized Variables
@property (nonatomic, strong) gameLogic *gameBoard;
@property (nonatomic, strong) gameData *gData;

- (IBAction) pressedContinueGame: (id) sender;
- (IBAction) pressedNewGame: (id) sender;
- (IBAction) backButton:(id)sender;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andBoard:(id)game andData:(gameData *)data;

@end
